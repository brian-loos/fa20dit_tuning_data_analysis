import pandas as pd
import numpy as np
import pickle as pkl
import glob, os,time
from matplotlib import pyplot as plt
from scipy.spatial import KDTree
from scipy.cluster.vq import vq, kmeans, whiten
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from torch.utils.data.dataset import random_split
import torch.nn.functional as F
from itertools import count
import gc

gc.collect()
class Map:
    def __init__(self): #empty constructor
        self.Table = []
        self.x_axis = []
        self.y_axis = []
        self.x_axis_label = ''
        self.y_axis_label = ''
        self.x_axis_scaled = []
        self.y_axis_scaled = []
        #self.rescale()
        self.scale = 0
    def rescale(self):
        s1 = np.size(self.x_axis)
        x1 = np.array(self.x_axis[:s1-1])
        x2 = np.array(self.x_axis[1:])
        s2 = np.size(self.y_axis)
        y1 = np.array(self.y_axis[:s2-1])
        y2= np.array(self.y_axis[1:])
        avg_x = sum(x2-x1)/(s1-1)
        avg_y = sum(y2-y1)/(s2-1)
        #want to scale up x to meet y, should work well in general
        scale = avg_y/avg_x
        self.scale = scale
        self.x_axis_scaled = self.x_axis*scale
        self.y_axis_scaled = self.y_axis
class Feedforward(torch.nn.Module):
    def __init__(self,input_size,hidden_size):
        super(Feedforward, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.fc1 = torch.nn.Linear(self.input_size, self.hidden_size)
        self.sig = torch.nn.Sigmoid()
        self.fc2 = torch.nn.Linear(self.hidden_size,hidden_size//10) #single output
        self.fc3 = torch.nn.Linear(hidden_size//10,1)
        self.relu = torch.nn.ReLU()
    def forward(self,x):
        hidden = self.fc1(x)
        relu = self.relu(hidden)
        out1 = self.fc2(relu)
        out1 = self.sig(out1)
        output = self.fc3(out1)
        #output = self.sig(output)#add another layer
        return output
class Feedforward_2(torch.nn.Module):
    def __init__(self,input_size,hidden_size):
        super(Feedforward_2, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.fc1 = torch.nn.Linear(self.input_size, self.hidden_size)
        self.sig = torch.nn.Sigmoid()
        self.fc2 = torch.nn.Linear(self.hidden_size, self.hidden_size)
        self.fc3 = torch.nn.Linear(self.hidden_size,hidden_size//10) #single output
        self.fc4 = torch.nn.Linear(hidden_size//10,1)
        self.relu = torch.nn.CELU()
    def forward(self,x):
        hidden = self.fc1(x)
        relu = self.sig(hidden)
        out1 = self.fc3(relu)
        out1 = self.relu(out1)
        output = self.fc4(out1)
        #output = self.sig(output)#add another layer
        return output

def make_train_step(model, loss_fn, optimizer):
    # Builds function that performs a step in the train loop
    def train_step(x, y):
        # Sets model to TRAIN mode
        model.train()
        # Makes predictions
        yhat = model(x)
        # Computes loss
        loss = loss_fn(y, yhat)
        # Computes gradients
        loss.backward()
        # Updates parameters and zeroes gradients
        optimizer.step()
        optimizer.zero_grad()
        # Returns the loss
        return loss.item()

    # Returns the function that will be called inside the train loop
    return train_step

def init_maf_calib():#returns dataframe with maf-calibration
    os.chdir('./Engine_Maps')
    fname = 'maf-calibration.csv'
    while True:#check if file exists
        if os.path.exists('./'+fname):
            break
        else:
            print(fnane + ' that file does not exist')
            fname = input('Please input maf-calibration file name')

    calib_file = pd.DataFrame(pd.read_csv(fname,sep = ',', header = 0))
    #print(logfile)

    os.chdir('..')
    return calib_file
def init_torque_ratio():
    os.chdir('./Engine_Maps')
    fname = 'torque_ratio_base.csv'
    while True:#check if file exists
        if os.path.exists('./'+fname):
            break
        else:
            print(fnane + ' that file does not exist')
            fname = input('Please input maf-calibration file name')

    calib_file = pd.DataFrame(pd.read_csv(fname,sep = ',', header = 0))
    #print(logfile)

    os.chdir('..')
    return calib_file
def init_table(fname):
    os.chdir('./Engine_Maps')
    while True:#check if file exists
        if os.path.exists('./'+fname):
            break
        else:
            print(fnane + ' that file does not exist')
            fname = input('Please input maf-calibration file name')

    table = pd.DataFrame(pd.read_csv(fname, sep = ',', header = 0))

    x_axis = table[table.columns[0]].to_numpy()
    x_axis = x_axis[~np.isnan(x_axis)]
    x_axis_label = table.columns[0]
    y_axis = table[table.columns[1]].to_numpy()
    y_axis = y_axis[~np.isnan(y_axis)]
    y_axis_label = table.columns[1]
    table.drop(table.columns[[0,1]],axis = 1, inplace = True)
    table = table.to_numpy()
    #print(table)

    os.chdir('..')
    return x_axis_label, y_axis_label,x_axis, y_axis, table

#NN stuff
def learn_MAF_V(eps):#given Throtte position sensor, boost pressure, and maf voltage
    os.chdir('..')#go to new directory
    os.chdir('./Datalogs/ver_6_alt')

    #logfile_name = 'datalog7.csv'
    #datalog = pd.DataFrame(pd.read_csv(logfile_name, sep = ',', header = 0))
    logs = []
    for file in glob.glob("datalog*.csv"):
        print(file)
        temp = pd.DataFrame(pd.read_csv(file, sep = ',', header = 0))
        logs.append(temp)
    #get rid of this thing
    #now we have two seperate lists, time to sort them into two seperate dataframes
    datalog = pd.concat(logs, ignore_index = True)
    t = datalog['Time (sec)']
    boost = datalog['Boost (psi)']
    TPS = datalog['Throttle Pos (%)']
    MAF_V = datalog['MAF Volts (V)']

    if False:
        fig = plt.figure()
        ax = fig.add_subplot(2,1,1)
        ax.plot(t,boost, label ='boost (psi)', color = 'black')
        ax_right = ax.twinx()
        ax_right.plot(t, TPS, label = 'Throttle Position (%)', color = 'red')
        ax.legend()
        ax_right.legend()
        ax = fig.add_subplot(2,1,2)
        ax.plot(t,MAF_V, label = 'MAF V')
        ax.legend()
        plt.show()
    #want to suppose the transfer function has some reasonable form
    #     f(p,tps) = (a*p + b)(polynomial approximant in tps)
    # we want to try and find this relationship using an ANN

    #T1, T2,T3, error = learning_ANN_multi(list(zip(TPS,boost)),np.array(MAF_V),3,10,np.size(TPS))
    #T1,T2, error = learning_ANN(list(zip(TPS,boost)), np.array(MAF_V), 10, np.size(TPS))
    #T1,T2,error = learning_RBF(list(zip(TPS,boost)), np.array(MAF_V), 5, np.size(TPS))
    #T1,T2,error = learning_RBF([(10,1),(200,3),(100,100),(0,1),(4,1443),(100,200),(30,20),(100,24)],np.array([1 , 4, 5, 6, 5, 6, 3, 1]), 4,8)
    training_inputs = np.array(list(zip(TPS,boost)))
    training_outputs = np.array(MAF_V)
    in_data = torch.FloatTensor(training_inputs)
    out_data = torch.FloatTensor(training_outputs)
    #create a dataset for batch descent
    dataset= TensorDataset(in_data, out_data)
    data_size = out_data.size()[0]
    print(data_size)
    training_dataset, val_dataset  = random_split(dataset, [int(data_size*.8),int(data_size*.2)])
    train_loader = DataLoader(dataset = training_dataset, batch_size = 20000, shuffle = True)
    val_loader = DataLoader(dataset = val_dataset, batch_size = 2000)
    #check for cuda
    if torch.cuda.is_available():
        dev = "cuda:0"
    else:
        dev = "cpu"
    torch.cuda.empty_cache()
    device = torch.device(dev)
    #in_data=in_data.to(device)
    #out_data=out_data.to(device)
    torch.cuda.empty_cache()
    M = 5000
    model = Feedforward(2,M)
    model.cuda()
    #creiterion = F.smooth_l1_loss()
    #optimizer = torch.optim.SGD(model.parameters(), lr = 0.01,momentum = .9) #set learning rate

    model.eval()
    #y_pred = model(in_data)

    optimizer = torch.optim.SGD(model.parameters(),lr = .01, momentum = .9)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False)
    losses = []
    run_loss = []
    val_losses = []
    #before_train = F.smooth_l1_loss(y_pred.squeeze(),out_data)
    #print('test loss before training', before_train.item())
    prev_loss = 1e6
    #prev_loss = before_train.item()
    ct = 0
    for index in count(1):
        torch.cuda.empty_cache()
        #train repeatedly using same data?
        for in_batch, out_batch in train_loader:
            in_batch = in_batch.to(device)
            out_batch = out_batch.to(device)
            optimizer.zero_grad()
            y_pred = model(in_batch)
            output = F.smooth_l1_loss(y_pred.squeeze(),out_batch)
            loss = output.item()
            output.backward()
            optimizer.step()

            losses.append(loss)
            run_loss.append(loss)
            #print(in_batch.size(), out_batch.size(),loss, index)
        with torch.no_grad():
            for x_val, y_val in val_loader:
                x_val = x_val.to(device)
                y_val = y_val.to(device)
                model.eval()
                yhat = model(x_val)
                val_loss = F.smooth_l1_loss(yhat.squeeze(),y_val)
                val_losses.append(val_loss.item())
        #torch.cuda.empty_cache()
        #compute average loss
        avg_loss = sum(run_loss)/len(run_loss)
        print(avg_loss,torch.cuda.memory_reserved(device)/(1e9),'maf model')
        #Use this to update a scheduler
        scheduler.step(avg_loss)
        run_loss = []
        if avg_loss < eps:
            break

    os.chdir('..')
    os.chdir('..')
    os.chdir('./Engine Modeling')
    fname = "MAF_model.pkl"
    PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),fname)
    torch.save(model.state_dict(), PATH)

    #10 nodes seem to work well enough
    #test over first 100 time points
    #output = T_OUT*temp, temp = logsig(T_IN*input)
    #forward propogate
    #load new data

    #forward propogation
    #forward prop for RBF
    #print(np.array(list(zip(TPS,boost))))
    """for x_val, y_val in val_loader:
        x_val = x_val.to(device)
        y_val = y_val.to(device)
        y_pred = model(x_val)
        y_pred = y_pred.cpu()
        y_val = y_val.cpu()
        #temp = gaussian(np.dot(list(zip(TPS,boost)),T1))
        #output =np.dot(temp, T2)
        #temp1 = logsig(np.dot(list(zip(TPS,boost)),T1))#works
            #print(np.shape(temp1))
        #temp2 = logsig(np.dot(temp1,T2))
            #print(np.shape(temp2))
        #output =np.dot(temp2, T3)
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        ax.plot(y_pred.detach().numpy(), label = 'output')
        ax.plot(y_val.detach().numpy(), label = 'MAF V')
        ax.legend()
        ax.set_title('ANN test results')
        plt.show()

    """
    #print(error)


    return model, loss

def learn_RPM_delta(eps): #want to learn rpm delta under driving conditions
    #we want to train a NN to predict instantenous RPM increase given LOAD and current RPM
    #along with maybe some other values
    os.chdir('..')#go to new directory
    os.chdir('./Datalogs/ver_6_alt')

    ####Can only run on a single datalog at a time
    batch_data=[]
    for file in glob.glob("datalog*.csv"):
        #print(file)
        datalog = pd.DataFrame(pd.read_csv(file, sep = ',', header = 0))
        t = datalog['Time (sec)'].to_numpy()
        RPM = datalog['RPM (RPM)'].to_numpy()
        boost = datalog['Boost (psi)'].to_numpy()
        TPS = datalog['Throttle Pos (%)'].to_numpy()
        accel_pos = datalog["Accel Position (%)"].to_numpy()
        calc_load = datalog['Calculated Load (g/rev)'].to_numpy()
        gear_pos = datalog["Gear Position (Gear)"].to_numpy()

        idx = np.size(gear_pos)-1
        #condition the RPM signal
        max = 700
        RPM = RPM/max#condition RPM data

        #first precondition the data as much as possible
        #want to get rid of idle (ie not on throttle) (TPS = 0%) remove all
        #want to get rid of shifts (up and down) remove like some radius around shifts
        #getting ride of 0% accel_pos
        #for this to work we are going to need to batch

        locs = np.where(accel_pos == 0)
        #also want to find where shifts occur
        dgear_pos = gear_pos[1:]-gear_pos[0:idx]
        shift_locs = np.where(dgear_pos != 0)
        #also want to add padding
        width = 20
        for loc in shift_locs[0]:
            lb = loc-width
            ub = loc+width
            print(lb,ub)
            if lb < 0:
                lb = 0
            if ub > idx:
                ub = idx
            locs = np.append(locs,np.arange(lb, ub))
        #print(np.sort(locs))
        min_length = width*2
        locs = np.unique(locs)
        if locs[np.size(locs)-1] == idx:
            locs = np.delete(locs, np.size(locs)-1)


        N = np.size(t)
        dt = t[1:]-t[0:N-1]
        drpm = RPM[1:]-RPM[0:N-1]
        t = t[0:N-1]
        RPM = RPM[0:N-1]
        calc_load = calc_load[0:N-1]
        #dgear_pos = gear_pos[1:]-gear_pos[0:N-1]
        gear_pos = gear_pos[0:N-1]
        accel_pos = accel_pos[0:N-1]
        TPS = TPS[0:N-1]
        boost = boost[0:N-1]
        #accel_pos = accel_pos[0:N-1]
        t = np.delete(t, locs)
        dt = np.delete(dt,locs)
        RPM = np.delete(RPM,locs)
        drpm = np.delete(drpm,locs)
        calc_load = np.delete(calc_load,locs)
        gear_pos = np.delete(gear_pos, locs)
        accel_pos = np.delete(accel_pos,locs)
        TPS = np.delete(TPS,locs)
        boost = np.delete(boost,locs)

        data =[t,dt,drpm,RPM,calc_load,gear_pos,TPS,boost]
        #print(data.shape)
        #print(batch_data)
        if len(batch_data) == 0:
            batch_data = data
        else:
            for i in range(len(data)):
                batch_data[i] = np.append(batch_data[i],data[i])

    if torch.cuda.is_available():
        dev = "cuda:0"
    else:
        dev = "cpu"
    #send to GPU
    torch.cuda.empty_cache()
    #setup the training data
    training_inputs = np.array(list(zip(batch_data[1], batch_data[3], batch_data[4], batch_data[5], batch_data[6], batch_data[7])))
    training_outputs = batch_data[2]
    in_data = torch.FloatTensor(training_inputs)
    out_data = torch.FloatTensor(training_outputs)
    #create a dataset for batch descent
    dataset= TensorDataset(in_data, out_data)
    data_size = out_data.size()[0]
    print(data_size)
    training_dataset, val_dataset  = random_split(dataset, [int(data_size*.8),data_size-int(data_size*.8)])
    train_loader = DataLoader(dataset = training_dataset, batch_size = 20000, shuffle = True)
    val_loader = DataLoader(dataset = val_dataset, batch_size = 2000)
    device = torch.device(dev)
    M = 5000
    model = Feedforward_2(6,M)#4 input, M size of hidden layer
    model.cuda()
    #creiterion = F.smooth_l1_loss()
    optimizer = torch.optim.SGD(model.parameters(), lr = .01,momentum = .9) #set learning rate using stochastic gradient descent
    #optimizer = torch.optim.Rprop(model.parameters(), lr = .01)
    model.eval()
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False)
    losses = []
    run_loss = []
    val_losses = []

    for i in count(1):

        for in_batch, out_batch in train_loader:
            in_batch = in_batch.to(device)
            out_batch = out_batch.to(device)
            optimizer.zero_grad()
            y_pred = model(in_batch)
            output = F.smooth_l1_loss(y_pred.squeeze(),out_batch)
            loss = output.item()
            output.backward()
            optimizer.step()

            losses.append(loss)
            run_loss.append(loss)
            #print(in_batch.size(), out_batch.size(),loss, index)
        with torch.no_grad():
            for x_val, y_val in val_loader:
                x_val = x_val.to(device)
                y_val = y_val.to(device)
                model.eval()
                yhat = model(x_val)
                val_loss = F.smooth_l1_loss(yhat.squeeze(),y_val)
                val_losses.append(val_loss.item())
        #torch.cuda.empty_cache()
        #compute average loss
        avg_loss = sum(run_loss)/len(run_loss)
        print(avg_loss,torch.cuda.memory_reserved(device)/(1e9),"rpm model")
        #Use this to update a scheduler
        scheduler.step(avg_loss)
        run_loss = []
        if avg_loss < eps:
            break
    os.chdir('..')
    os.chdir('..')
    os.chdir('./Engine Modeling')
    fname = "RPM_model.pkl"
    PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),fname)
    torch.save(model.state_dict(), PATH)


    """logfile_name = 'test_data_2.csv'
    datalog = pd.DataFrame(pd.read_csv(logfile_name, sep = ',', header = 0))

    t   = datalog['Time (sec)'].to_numpy()
    RPM = datalog['RPM (RPM)'].to_numpy()
    boost = datalog['Boost (psi)'].to_numpy()
    TPS = datalog['Throttle Pos (%)'].to_numpy()
    #accel_pos = datalog["Accel Position (%)"].to_numpy()
    calc_load = datalog['Calculated Load (g/rev)'].to_numpy()
    gear_pos = datalog["Gear Position (Gear)"].to_numpy()
    max = 700#use as 7000/100 for consistency between files
    RPM = RPM/max
    N = np.size(t)
    dt = t[1:]-t[0:N-1]
    drpm = RPM[1:]-RPM[0:N-1]
    t = t[0:N-1]
    RPM = RPM[0:N-1]
    calc_load = calc_load[0:N-1]
    #dgear_pos = gear_pos[1:]-gear_pos[0:N-1]
    gear_pos = gear_pos[0:N-1]
    #accel_pos = accel_pos[0:N-1]
    TPS = TPS[0:N-1]
    boost = boost[0:N-1]
    #accel_pos = accel_pos[0:N-1]

    training_inputs = np.array(list(zip(dt,RPM,calc_load,gear_pos, TPS, boost)))
    in_data = torch.FloatTensor((training_inputs))
    training_outputs = drpm
    in_data=in_data.to(device)
    out_data = torch.FloatTensor((training_outputs))
    out_data=out_data.to(device)

    y_pred = model(in_data)

    output = F.smooth_l1_loss(y_pred.squeeze(),out_data)
    loss = output.item()

    y_pred = y_pred.cpu()
    print(loss)
    fig = plt.figure()
    ax = fig.add_subplot(2,1,1)
    ax.plot(t,drpm*max, label ='dRPM actual', color = 'black')
    ax.plot(t,y_pred.detach().numpy()*max , label = 'dRPM predicted', color = 'red')
    #ax_right = ax.twinx()
    #print(y_pred.detach().numpy())
    #ax_right.plot(t,y_pred.detach().numpy() , label = 'dRPM predicted', color = 'red')
    ax.legend()

    ax = fig.add_subplot(2,1,2)
    ax.plot(t,gear_pos, label = 'gear position', color = 'black')
    ax_right= ax.twinx()
    ax_right.plot(t,TPS, label = 'accel position', color ='red')
    ax.legend()
    ax_right.legend()

    #ax_right.legend()
    plt.show()"""
    #trained network
    #plot current network performance


    return model, loss
def learn_boost_delta(eps):#want to learn boost delta under driving conditions
    #we want to train a NN to predict instantenous boost increase given LOAD and current RPM
    #along with maybe some other values
    ####identitcal to learn_RPM_delta
    os.chdir('..')#go to new directory
    os.chdir('./Datalogs/ver_6_alt')

    ####Can only run on a single datalog at a time
    batch_data=[]

    ##normalization limits


    for file in glob.glob("datalog*.csv"):
        #print(file)
        datalog = pd.DataFrame(pd.read_csv(file, sep = ',', header = 0))
        t = datalog['Time (sec)'].to_numpy()
        RPM = datalog['RPM (RPM)'].to_numpy()
        boost = datalog['Boost (psi)'].to_numpy()
        TPS = datalog['Throttle Pos (%)'].to_numpy()
        accel_pos = datalog["Accel Position (%)"].to_numpy()
        calc_load = datalog['Calculated Load (g/rev)'].to_numpy()
        gear_pos = datalog["Gear Position (Gear)"].to_numpy()
        target_boost = datalog["Target Boost Final Abs (psi)"].to_numpy()
        idx = np.size(gear_pos)-1
        #first precondition the data as much as possible
        #print(np.max(calc_load))
        #want to get rid of idle (ie not on throttle) (TPS = 0%) remove all
        #want to get rid of shifts (up and down) remove like some radius around shifts
        #getting ride of 0% accel_pos
        #for this to work we are going to need to batch
        #use same conditioning
        locs = np.where(accel_pos == 0)
        #also want to find where shifts occur
        dgear_pos = gear_pos[1:]-gear_pos[0:idx]
        shift_locs = np.where(dgear_pos != 0)
        #also want to add padding
        width = 20
        for loc in shift_locs[0]:
            lb = loc-width
            ub = loc+width
            print(lb,ub)
            if lb < 0:
                lb = 0
            if ub > idx:
                ub = idx
            locs = np.append(locs,np.arange(lb, ub))
        #print(np.sort(locs))
        min_length = width*2
        locs = np.unique(locs)
        if locs[np.size(locs)-1] == idx:
            locs = np.delete(locs, np.size(locs)-1)
        max = 700
        RPM = RPM/max

        N = np.size(t)
        dt = t[1:]-t[0:N-1]
        t = t[0:N-1]
        RPM = RPM[0:N-1]
        calc_load = calc_load[0:N-1]
        #dgear_pos = gear_pos[1:]-gear_pos[0:N-1]
        gear_pos = gear_pos[0:N-1]
        accel_pos = accel_pos[0:N-1]
        TPS = TPS[0:N-1]
        dboost = boost[1:]-boost[0:N-1]
        #dboost_target = target_boost[1:] - target_boost[0:N-1]
        boost = boost[0:N-1]
        target_boost = target_boost[0:N-1]
        #accel_pos = accel_pos[0:N-1]
        t = np.delete(t, locs)
        dt = np.delete(dt,locs)
        RPM = np.delete(RPM,locs)
        calc_load = np.delete(calc_load,locs)
        gear_pos = np.delete(gear_pos, locs)
        accel_pos = np.delete(accel_pos,locs)
        TPS = np.delete(TPS,locs)
        boost = np.delete(boost,locs)
        dboost = np.delete(dboost,locs)
        target_boost = np.delete(target_boost,locs)


        data =[t,dt,RPM,calc_load,gear_pos,TPS,boost,target_boost,dboost]
        #print(data.shape)
        #print(batch_data)
        if len(batch_data) == 0:
            batch_data = data
        else:
            for i in range(len(data)):
                batch_data[i] = np.append(batch_data[i],data[i])
    torch.cuda.empty_cache()
    if torch.cuda.is_available():
        dev = "cuda:0"
    else:
        dev = "cpu"
    #send to GPU
    device = torch.device(dev)

    training_inputs = np.array(list(zip(batch_data[1], batch_data[2], batch_data[3], batch_data[4], batch_data[5], batch_data[6],batch_data[7])))
    #training_inputs = np.array(list(zip(RPM,calc_load,dt, TPS, boost, gear_pos)))
    training_outputs = batch_data[8]
    in_data = torch.FloatTensor(training_inputs)
    out_data = torch.FloatTensor(training_outputs)
    #create a dataset for batch descent
    dataset= TensorDataset(in_data, out_data)
    data_size = out_data.size()[0]
    #print(data_size)
    training_dataset, val_dataset  = random_split(dataset, [int(data_size*.8), data_size-int(data_size*.8)])
    train_loader = DataLoader(dataset = training_dataset, batch_size = 20000, shuffle = True)
    val_loader = DataLoader(dataset = val_dataset, batch_size = 2000)
    device = torch.device(dev)
    M = 5000
    model = Feedforward_2(7,M)#4 input, M size of hidden layer
    model.cuda()
    #creiterion = F.smooth_l1_loss()
    optimizer = torch.optim.SGD(model.parameters(), lr = .01,momentum = .9) #set learning rate using stochastic gradient descent
    #optimizer = torch.optim.Rprop(model.parameters(), lr = .01)
    model.eval()
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=10, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False)
    losses = []
    run_loss = []
    val_losses = []
    lr = .01
    m = .9
    for i in count(1):
        for in_batch, out_batch in train_loader:
            in_batch = in_batch.to(device)
            out_batch = out_batch.to(device)
            optimizer.zero_grad()
            y_pred = model(in_batch)
            output = F.smooth_l1_loss(y_pred.squeeze(),out_batch)
            loss = output.item()
            output.backward()
            optimizer.step()

            losses.append(loss)
            run_loss.append(loss)
            #print(in_batch.size(), out_batch.size(),loss, index)
        with torch.no_grad():
            for x_val, y_val in val_loader:
                x_val = x_val.to(device)
                y_val = y_val.to(device)
                model.eval()
                yhat = model(x_val)
                val_loss = F.smooth_l1_loss(yhat.squeeze(),y_val)
                val_losses.append(val_loss.item())
        #torch.cuda.empty_cache()
        #compute average loss
        avg_loss = sum(run_loss)/len(run_loss)
        print(avg_loss,torch.cuda.memory_reserved(device)/(1e9),"boost model")
        #Use this to update a scheduler
        scheduler.step(avg_loss)
        run_loss = []
        if avg_loss < eps:
            break

    os.chdir('..')
    os.chdir('..')
    os.chdir('./Engine Modeling')
    fname = "boost_model.pkl"
    PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)),fname)
    torch.save(model.state_dict(), PATH)

    """logfile_name = 'test_data_2.csv'
    datalog = pd.DataFrame(pd.read_csv(logfile_name, sep = ',', header = 0))

    t   = datalog['Time (sec)'].to_numpy()
    RPM = datalog['RPM (RPM)'].to_numpy()
    boost = datalog['Boost (psi)'].to_numpy()
    TPS = datalog['Throttle Pos (%)'].to_numpy()
    #accel_pos = datalog["Accel Position (%)"].to_numpy()
    calc_load = datalog['Calculated Load (g/rev)'].to_numpy()
    gear_pos = datalog["Gear Position (Gear)"].to_numpy()
    target_boost = datalog["Target Boost Final Abs (psi)"].to_numpy()
    max = 700
    RPM = RPM/max

    N = np.size(t)
    dt = t[1:]-t[0:N-1]
    t = t[0:N-1]
    RPM = RPM[0:N-1]
    calc_load = calc_load[0:N-1]
    #dgear_pos = gear_pos[1:]-gear_pos[0:N-1]
    gear_pos = gear_pos[0:N-1]
    #accel_pos = accel_pos[0:N-1]
    TPS = TPS[0:N-1]
    dboost = boost[1:]-boost[0:N-1]
    boost = boost[0:N-1]
    target_boost = target_boost[0:N-1]
    #accel_pos = accel_pos[0:N-1]

    #normalize for NN
    training_inputs = np.array(list(zip(dt,RPM,calc_load,gear_pos,TPS,boost,target_boost)))
    in_data = torch.FloatTensor((training_inputs))
    training_outputs = dboost
    in_data=in_data.to(device)
    out_data = torch.FloatTensor((training_outputs))
    out_data=out_data.to(device)

    y_pred = model(in_data)

    output = F.smooth_l1_loss(y_pred.squeeze(),out_data)
    loss = output.item()

    #denormalize

    y_pred = y_pred.cpu()
    print(loss)
    fig = plt.figure()
    ax = fig.add_subplot(2,1,1)
    ax.plot(t,dboost, label ='dboost actual', color = 'black')
    ax.plot(t,y_pred.detach().numpy() , label = 'dboost predicted', color = 'red')
    #ax_right = ax.twinx()
    #print(y_pred.detach().numpy())
    #ax_right.plot(t,y_pred.detach().numpy() , label = 'dRPM predicted', color = 'red')
    ax.legend()

    ax = fig.add_subplot(2,1,2)
    ax.plot(t,gear_pos, label = 'gear position', color = 'black')
    ax_right= ax.twinx()
    ax_right.plot(t,TPS, label = 'accel position', color ='red')
    ax.legend()
    ax_right.legend()

    #ax_right.legend()
    plt.show()"""
    #trained network
    #plot current network performance

    return model, loss

#initialize neural networks
def approx_1d(R,vals,query):
    nn, err = simple_nearest_neighbor(R,query)#find the simple nearest neighbor
    loc = np.where(R == nn) #find the nearest neighbor value
    loc = loc[0] #only need the singular value
    if err <= 0:
        if loc == np.size(vals)-1:
            #this is the last value, no interpolant possible
            return vals[loc]
        else:
            l1 = loc+1
            dl = R[l1]- R[loc] #find the delta
            q1 = np.abs(err)/dl
            q2 = 1-q1
            return q1*vals[l1]+q2*vals[loc]
    elif err > 0:
            if loc == 0:
                return vals[loc]
            else:
                l1 = loc-1
                dl = R[loc]-R[l1]
                q1 = np.abs(err)/dl
                q2 = 1-q1
                return q2*vals[l1] + q1*vals[loc]


torch.cuda.empty_cache()
str = input('Please select option \n (1) re-learn neural networks \n (2) load existing files (requires .pkl files) \n')
if str == '1':
    MAF_model, MAF_loss = learn_MAF_V(6e-3)#creates a neural net that predicts MAF voltage based off of boost and TPS
    rpm_model, rpm_loss = learn_RPM_delta(2e-4)
    boost_model, boost_loss = learn_boost_delta(35e-3)
elif str == '2':

    MAF_model = Feedforward(2,5000)
    MAF_model.load_state_dict(torch.load("./MAF_model.pkl"))
    MAF_model.eval()
    rpm_model = Feedforward_2(6,5000)
    rpm_model.load_state_dict(torch.load("./RPM_model.pkl"))
    rpm_model.eval()
    boost_model = Feedforward_2(7,5000)
    boost_model.load_state_dict(torch.load("./boost_model.pkl"))
    boost_model.eval()


    MAF_model.eval()
    rpm_model.eval()
    boost_model.eval()
    #this works and sets them up for evaluation mode
    #allow evaluations
#load in some data log

#load some test data
datalog = pd.DataFrame(pd.read_csv("datalog9.csv",sep = ',', header = 0))
t   = datalog['Time (sec)'].to_numpy()
RPM = datalog['RPM (RPM)'].to_numpy()
boost = datalog['Boost (psi)'].to_numpy()
MAF_V = datalog['MAF Volts (V)'].to_numpy()
TPS = datalog['Throttle Pos (%)'].to_numpy()
accel_pos = datalog["Accel Position (%)"].to_numpy()
calc_load = datalog['Calculated Load (g/rev)'].to_numpy()
gear_pos = datalog["Gear Position (Gear)"].to_numpy()
target_boost = datalog["Target Boost Final Abs (psi)"].to_numpy()



os.chdir('..')
os.chdir('..')
os.chdir('./Engine Modeling')

#we can just see how accurate the MAF time series prediction will be.

#need to create something that will predict instantaneous RPM change based off of current RPM and current load
#we can teach off of a single log file with gear position as well.
#if we use RPM, GEAR POSITION, and LOAD as inputs, then we should be able to reasonably train the NN. It may also be reasonable to conclude that if this fails,
#then we may have to use VE parameters to help more precisely nail down the desired parameter space.
#we finally need to determine a map, that predicts boost levels based off of target boost, instantaneous engine speed and load. With this we should be able to run the entire
#feedback system off of just accelerator position and the engine maps

#the RPM and boost maps can be fine tuned with the engine fueling and spark maps, but I'm not quite sure how to make use of them yet
#these define primarily power efficiency. (ie leaner burn ---> more power to a point, more advance ---> more power to a point)
#try to determine an active model for this
#2018 WRX gearing stock
# gear      ratio      final_ratio (ratio*f_drive)
# 1st       3.454      14.199394
# 2nd       1.947       8.0041117
# 3rd       1.296       5.327856
# 4th       0.972       3.995892
# 5th       0.78        3.20658
# 6th       0.666       2.737926
# rev       3.636      14.947596
# f_drive   4.111
#gearing = torch.FloatTensor([3.454, 1.947, 1.296, 0.972, .78 , .666, .3.636])#create a library with this stuff in in
#f_drive = torch.FloatTensor([4.111])#final drive
#f_gearing = f_drive*gearing
curb_weight = 1495
#curb weight:          1495 kg
#on 235/40R17 wheels ---> approx 17" rims with ~94mm sidewall ---> 309.8 mm radius wheels w/ tires
#if we assume no slippage, we can compute the average instantenous force at the wheels based off of change in RPM*radius? then also account for car weight
#we could also compute and interpolation of average kinetic energy of the car then divide by the timestep to get an estimate of instantaneous power, this would give a fairly decent approximate too
wheel_rad = .3098 #in meters
wheel_cir = torch.FloatTensor([2*wheel_rad*np.pi]) #wheel circumference


if False:
    t_ratio = init_torque_ratio()#initialize the torque ratio data frame
    est = approx_1d(t_ratio['RPM'].to_numpy(),t_ratio['Torque Ratio Base'].to_numpy(), 6148)
    print(est)#estimated desired torque ratio
    #initialize accelartor ---> requested torque tree
    RT_map = Map()#requested torque tree
    RT_map.x_axis_label, RT_map.y_axis_label, RT_map.x_axis, RT_map.y_axis, RT_map.Table = init_table('requested_torque.csv')
    RT_map.rescale()
    X,Y = np.meshgrid(RT_map.x_axis_scaled, RT_map.y_axis_scaled)
    RT_tree = KDTree(list(zip(X.ravel(), Y.ravel())))
    #print(RT_tree.data)
    #initialize requested torque ratio ---> target throttle angle
    TTA_map = Map()
    TTA_map.x_axis_label, TTA_map.y_axis_label, TTA_map.x_axis, TTA_map.y_axis, TTA_map.Table = init_table('target_throttle_main_TGV_open_A.csv')
    TTA_map.rescale()
    X,Y = np.meshgrid(TTA_map.x_axis_scaled, TTA_map.y_axis_scaled)
    #we need to rescale grids and shit
    TTA_tree = KDTree(list(zip(X.ravel(),Y.ravel())))
    dd, ii = TTA_tree.query([1.0*TTA_map.scale,2562],k =4)
    print(TTA_tree.data[ii]/TTA_map.scale)
    print(TTA_tree.data[ii])

    #make some test data
    accel_pos_curr = np.arange(0,100,.1)#perfectly linear throttle actuation
