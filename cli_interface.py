#cli interface for everything that was running before:
#must run this from the home directory
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import glob, os
import manipulations
import pyglet
import pickle
import time

class logData:
    def __init__(self, input_data,name):
        self.rawData = input_data
        self.clData = []
        self.olData = []
        self.name = name

class Data:
    def __init__(self, params, log):
        self.data = {}
        for param in params: #params must be given as a list of strings, log corresponding Pandas Dataframe
            input_data = log[param].to_numpy() #get the raw data
            self.data[param] = logData(input_data, param) #should create a dictionary of names and values

def init_logfile():
    logs = []
    os.chdir('./Datalogs/')
    print("Please choose which version from following list")
    os.system('dir')
    ver = input()
    os.chdir('./'+ver)
    #make array of dataframes
    for file in glob.glob("*.csv"):
        print(file)
        temp = pd.DataFrame(pd.read_csv(file, sep = ',', header = 0))
        logs.append(temp)
    #now there are going to be different types of log files
    os.system('del *.pkl')
    #get rid of this thing
    #now we have two seperate lists, time to sort them into two seperate dataframes
    logfile = pd.concat(logs, ignore_index = True)          #--> without CL fueling switch
    #prune data to get only what is during warm engine operation
    oil_temp_cutoff = 180.0 #arbitary cutoff, could go higher
    oil_temp = logfile["Oil Temp (F)"].to_numpy()
    invalid_temp_locs = np.where(oil_temp < oil_temp_cutoff)
    logfile.drop(invalid_temp_locs[0], axis = 0,inplace = True)
    t = (len(logfile))*(1/15)#approximate dt
    if t > 60:
        print('logs represent approximately', t/60,' minutes')
    else:
        print('logs represent approximately ',t,' seconds' )

    #print(logfile)
    return logfile
def return_base():
    os.chdir('..')
    os.chdir('..')
def set_params(logfile):
    #list of desired parameters
    params = logfile.columns.values.tolist()
    for param in params:
        print(param)


    desired_params = ["AF Learning 1 (%)", "AF Correction 1 (%)", "MAF Corr (g/s)",
                    "MAF Volts (V)", "Calculated Load (g/rev)", "Closed Loop Sw (on/off)",
                      "AF Sens 1 Ratio (AFR)", "Comm Fuel Final (AFR)",
                      "CL Fuel Target (AFR)", "Feedback Knock (�)",
                      "Gear Position (Gear)", "RPM (RPM)", "Dyn Adv Mult (DAM)","Fine Knock Learn (�)",
                      "Inj Duty Cycle (%)","Wastegate Duty (%)","Throttle Pos (%)",
                      "Boost (psi)","Coolant Temp (F)","Intake Temp (F)","Intake Temp Manifold (F)",
                      "MAF VE (%)","Oil Temp (F)"]



    for param in desired_params:
        #check if in list
        if not (param in params):
            print(param+" is not in current logfile")
    data_set = Data(desired_params, logfile)#get the data
    locs =np.where(data_set.data["Closed Loop Sw (on/off)"].rawData == 'on')
    for key in data_set.data.keys():
        data_set.data[key].clData = data_set.data[key].rawData[locs]
    #initialize OL data
    locs =np.where(data_set.data["Closed Loop Sw (on/off)"].rawData == 'off')
    for key in data_set.data.keys():
        data_set.data[key].olData = data_set.data[key].rawData[locs]
    return data_set
def main():
    logfile = init_logfile()
    data_set = set_params(logfile)
    CL_args = [data_set.data["AF Learning 1 (%)"].clData, data_set.data["AF Correction 1 (%)"].clData,
                    data_set.data["MAF Corr (g/s)"].clData, data_set.data["MAF Volts (V)"].clData,
                     data_set.data["Calculated Load (g/rev)"].clData,
                    data_set.data["Closed Loop Sw (on/off)"].rawData, data_set.data["AF Sens 1 Ratio (AFR)"].clData,
                     data_set.data["Comm Fuel Final (AFR)"].clData, data_set.data["CL Fuel Target (AFR)"].clData]
    OL_args = [data_set.data["AF Sens 1 Ratio (AFR)"].olData, data_set.data["Comm Fuel Final (AFR)"].olData, data_set.data["MAF Corr (g/s)"].olData,
                data_set.data["MAF Volts (V)"].olData]
    AF_args_raw =[data_set.data["AF Learning 1 (%)"].rawData, data_set.data["AF Correction 1 (%)"].rawData]
    knock_args = [data_set.data["Feedback Knock (�)"].rawData, data_set.data["Gear Position (Gear)"].rawData,
                    data_set.data["RPM (RPM)"].rawData, data_set.data["Calculated Load (g/rev)"].rawData,
                    data_set.data["Dyn Adv Mult (DAM)"].rawData]

    offset_CL = manipulations.CL_MAF_calibration(*CL_args)
    offset_OL  = manipulations.OL_MAF_calibration(*OL_args)
    #want to make plots of where the data is in load/RPM state space
    manipulations.data_distribution(data_set.data["Calculated Load (g/rev)"].clData, data_set.data["Calculated Load (g/rev)"].olData,
                                    data_set.data["RPM (RPM)"].clData, data_set.data["RPM (RPM)"].olData)
    calib_args = [data_set.data["AF Learning 1 (%)"].rawData, data_set.data["AF Correction 1 (%)"].rawData, offset_CL, offset_OL,
                    data_set.data["Closed Loop Sw (on/off)"].rawData]
    manipulations.MAF_calibration_interp(*calib_args)
    manipulations.fuel_trim_distribution(*AF_args_raw)
    manipulations.knock_3d(*knock_args)
    manipulations.fkl_3d(data_set.data["Fine Knock Learn (�)"].rawData, data_set.data["Calculated Load (g/rev)"].rawData,
                            data_set.data["RPM (RPM)"].rawData)
    manipulations.duty_cycles(data_set.data["Inj Duty Cycle (%)"].rawData, data_set.data["Wastegate Duty (%)"].rawData)
    manipulations.temp_plots(data_set.data["Coolant Temp (F)"].rawData, data_set.data["Intake Temp (F)"].rawData, data_set.data["Intake Temp Manifold (F)"].rawData,data_set.data["Oil Temp (F)"].rawData)
    manipulations.MAF_VE_dist(data_set.data["MAF VE (%)"].rawData)
    
def plot_VE_dist():
    with open('MAF_VE_corr_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_CL_MAF():
    with open('CL_MAF_offsets.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_MAF_corr():
    with open('MAF_corr.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_fuel_dist():
    with open('fuel_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_knock():
    with open('knock.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_fkl():
    with open('fkl_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_coolant_dist():
    with open('coolant_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_int_dist():
    with open('int_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_int_man_dist():
    with open('int_man_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_oil_temp_dist():
    with open('oil_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()

def plot_temps():
    with open('temperature_time_series.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
