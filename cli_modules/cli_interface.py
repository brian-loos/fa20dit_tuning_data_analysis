                                   #cli interface for everything that was running before:
#must run this from the home directory
if False:
    print(None)
#   tracked parameters for me:
#   Time (sec),
#   AF Correction 1 (%),
#   AF Learning 1 (%),
#   AF Sens 1 Ratio (AFR),
#   Accel Position (%),
#   Aggr Start 1 Active (on/off),
#   Aggr Start 2 Active (on/off),
#   Boost (psi),
#   CL Fuel Target (AFR),
#   Calculated Load (g/rev),
#   Closed Loop Sw (on/off),
#   Comm Fuel Final (AFR),
#   Coolant Temp (F),
#   Dyn Adv Mult (DAM),
#   Dynamic Adv (�),
#   EGR Commanded (steps),
#   EGR Estimate (steps),
#   Feedback Knock (�),
#   Fine Knock Learn (�),
#   Fuel Mode (mode),
#   Gear Position (Gear),
#   Ignition Timing (�),
#   Inj Duty Cycle (%),
#   Inj PW (ms),
#   Inj Timing H Table (raw),
#   Intake Temp (F),
#   Intake Temp Manifold (F),
#   MAF Corr (g/s),
#   MAF VE (%),
#   MAF Volts (V),
#   Man Abs Press (psi),
#   Oil Temp (F),
#   Primary Ign (�),
#   RPM (RPM),
#   Req Torque (Nm),
#   TD Boost Error (psi),
#   TGV Map Ratio (mult),
#   Target Boost Final Abs (psi),
#   Throttle Pos (%),
#   Wastegate Duty (%),
#AP Info:[AP3-SUB-004 v1.7.4.0-17408][2018 USDM WRX MT COBB Custom Features Gen1][Reflash: advanced_timing_ver3.ptm - Realtime: advanced_timing_ver3.ptm]

import numpy as np#to hide info
import pandas as pd
import matplotlib.pyplot as plt
import glob, os
import manipulations
import pyglet
import pickle
import time

class logData:
    def __init__(self, input_data,name):
        self.rawData = input_data
        self.clData = []
        self.olData = []
        self.name = name

class Data:
    def __init__(self, params, log):
        self.data = {}
        for param in params: #params must be given as a list of strings, log corresponding Pandas Dataframe
            input_data = log[param].to_numpy() #get the raw data
            self.data[param] = logData(input_data, param) #should create a dictionary of names and values

def init_logfile():
    logs = []
    os.chdir('..')
    os.chdir('./Datalogs/')
    print("Please choose which version from following list")
    os.system('dir')
    ver = input()
    os.chdir('./'+ver)
    #make array of dataframes
    for file in glob.glob("*.csv"):
        print(file)
        temp = pd.DataFrame(pd.read_csv(file, sep = ',', header = 0))
        logs.append(temp)
    #now there are going to be different types of log files
    os.system('del *.pkl')
    #get rid of this thing
    #now we have two seperate lists, time to sort them into two seperate dataframes
    logfile = pd.concat(logs, ignore_index = True)          #--> without CL fueling switch
    #prune data to get only what is during warm engine operation
    oil_temp_cutoff = 180.0 #arbitary cutoff, could go higher
    oil_temp = logfile["Oil Temp (F)"].to_numpy()
    invalid_temp_locs = np.where(oil_temp < oil_temp_cutoff)
    logfile.drop(invalid_temp_locs[0], axis = 0,inplace = True)
    t = (len(logfile))*(1/15)#approximate dt
    if t > 60:
        print('logs represent approximately', t/60,' minutes')
    else:
        print('logs represent approximately ',t,' seconds' )

    #print(logfile)
    return logfile
def init_logfile_single():
    logs = []
    os.chdir('..')
    os.chdir('./Datalogs/')
    print("Please choose which version from following list")
    os.system('dir')
    ver = input()
    os.chdir('./'+ver)
    #make array of dataframes
    print("Please choose logfile from following list")
    os.system('dir')
    while True:
        fname = input()
        if os.path.exists('./'+fname):
            break
        else:
            print('that logfile does not exist')



    os.system('del *.pkl')
    #get rid of this thing
    #now we have two seperate lists, time to sort them into two seperate dataframes
    logfile = pd.DataFrame(pd.read_csv(fname,sep = ',', header = 0))
    #prune data to get only what is during warm engine operation
    oil_temp_cutoff = 180.0 #arbitary cutoff, could go higher
    oil_temp = logfile["Oil Temp (F)"].to_numpy()
    invalid_temp_locs = np.where(oil_temp < oil_temp_cutoff)
    logfile.drop(invalid_temp_locs[0], axis = 0,inplace = True)
    t = (len(logfile))*(1/15)#approximate dt
    if t > 60:
        print('logs represent approximately', t/60,' minutes')
    else:
        print('logs represent approximately ',t,' seconds' )

    #print(logfile)
    return logfile
def return_base():
    os.chdir('..')
    os.chdir('..')
    os.chdir('./cli_modules')
def set_params(logfile):
    #list of desired parameters
    params = logfile.columns.values.tolist()
    for param in params:
        print(param)
    desired_params = ["Time (sec)", "AF Learning 1 (%)", "AF Correction 1 (%)", "MAF Corr (g/s)",
                    "MAF Volts (V)", "Calculated Load (g/rev)", "Closed Loop Sw (on/off)",
                      "AF Sens 1 Ratio (AFR)", "Comm Fuel Final (AFR)",
                      "CL Fuel Target (AFR)", "Feedback Knock (�)",
                      "Gear Position (Gear)", "RPM (RPM)", "Dyn Adv Mult (DAM)","Fine Knock Learn (�)",
                     "Wastegate Duty (%)","Throttle Pos (%)",
                      "Boost (psi)","Coolant Temp (F)","Intake Temp (F)","Intake Temp Manifold (F)",
                      "MAF VE (%)","Oil Temp (F)","EGR Commanded (steps)","Ignition Timing (�)","Inj Timing H Table (raw)","Primary Ign (�)","Req Torque (Nm)",
                        "TD Boost Error (psi)","TGV Map Ratio (mult)","Target Boost Final Abs (psi)", "Throttle Pos (%)",  "EGR Estimate (steps)"]
    for param in desired_params:
        #check if in list
        if not (param in params):
            print(param+" is not in current logfile")
    data_set = Data(desired_params, logfile)#get the data
    locs =np.where(data_set.data["Closed Loop Sw (on/off)"].rawData == 'on')
    for key in data_set.data.keys():
        data_set.data[key].clData = data_set.data[key].rawData[locs]
    #initialize OL data
    locs =np.where(data_set.data["Closed Loop Sw (on/off)"].rawData == 'off')
    for key in data_set.data.keys():
        data_set.data[key].olData = data_set.data[key].rawData[locs]
    return data_set
def main():
    str = input("Please enter which version of the program to run: \n    (1)MAF correction analysis \n    (2)Knock & time series analysis \n")
    if str == '1': #prepares data set for specific plotting operations
        logfile = init_logfile()
        data_set = set_params(logfile)
        CL_args = [data_set.data["AF Learning 1 (%)"].clData, data_set.data["AF Correction 1 (%)"].clData,
                        data_set.data["MAF Corr (g/s)"].clData, data_set.data["MAF Volts (V)"].clData,
                         data_set.data["Calculated Load (g/rev)"].clData,
                        data_set.data["Closed Loop Sw (on/off)"].rawData, data_set.data["AF Sens 1 Ratio (AFR)"].clData,
                         data_set.data["Comm Fuel Final (AFR)"].clData, data_set.data["CL Fuel Target (AFR)"].clData]
        OL_args = [data_set.data["AF Sens 1 Ratio (AFR)"].olData, data_set.data["Comm Fuel Final (AFR)"].olData, data_set.data["MAF Corr (g/s)"].olData,
                    data_set.data["MAF Volts (V)"].olData]
        AF_args_raw =[data_set.data["AF Learning 1 (%)"].rawData, data_set.data["AF Correction 1 (%)"].rawData]
        knock_args = [data_set.data["Feedback Knock (�)"].rawData, data_set.data["Gear Position (Gear)"].rawData,
                        data_set.data["RPM (RPM)"].rawData, data_set.data["Calculated Load (g/rev)"].rawData,
                        data_set.data["Dyn Adv Mult (DAM)"].rawData]

        offset_CL = manipulations.CL_MAF_calibration(*CL_args)
        offset_OL  = manipulations.OL_MAF_calibration(*OL_args)
        #want to make plots of where the data is in load/RPM state space
        manipulations.data_distribution(data_set.data["Calculated Load (g/rev)"].clData, data_set.data["Calculated Load (g/rev)"].olData,
                                        data_set.data["RPM (RPM)"].clData, data_set.data["RPM (RPM)"].olData)
        calib_args = [data_set.data["AF Learning 1 (%)"].rawData, data_set.data["AF Correction 1 (%)"].rawData, offset_CL, offset_OL,
                        data_set.data["Closed Loop Sw (on/off)"].rawData]
        manipulations.MAF_calibration_interp(*calib_args)
        manipulations.fuel_trim_distribution(*AF_args_raw)
        manipulations.knock_3d(*knock_args)
        manipulations.fkl_3d(data_set.data["Fine Knock Learn (�)"].rawData, data_set.data["Calculated Load (g/rev)"].rawData,
                                data_set.data["RPM (RPM)"].rawData)
        manipulations.duty_cycles(data_set.data["Inj Duty Cycle (%)"].rawData, data_set.data["Wastegate Duty (%)"].rawData)
        manipulations.temp_plots(data_set.data["Coolant Temp (F)"].rawData, data_set.data["Intake Temp (F)"].rawData, data_set.data["Intake Temp Manifold (F)"].rawData,data_set.data["Oil Temp (F)"].rawData)
        manipulations.MAF_VE_dist(data_set.data["MAF VE (%)"].rawData)
    elif str == '2':#prepares data for knock and time sereies analysis
        logfile = init_logfile_single()#creates a single logfile
        data_set = set_params(logfile)#initializes the dataset
        #the tests that we want to perform in this program
        #(1) we want to find, then filter where the driving event of interest takes place
        #(2) we want to look at boost vs boost targets
        boost = data_set.data["Boost (psi)"].rawData
        target_boost = data_set.data["Target Boost Final Abs (psi)"].rawData
        TD_error = data_set.data["TD Boost Error (psi)"].rawData
        target_boost = target_boost - 14.69
        knock = data_set.data["Feedback Knock (�)"].rawData
        rpm = data_set.data["RPM (RPM)"].rawData
        load = data_set.data["Calculated Load (g/rev)"].rawData
        AFR = data_set.data["AF Sens 1 Ratio (AFR)"].rawData
        comm_AFR = data_set.data["Comm Fuel Final (AFR)"].rawData
        MAF  = data_set.data["MAF Corr (g/s)"].rawData
        ign_timing = data_set.data["Ignition Timing (�)"].rawData
        inj_timing = data_set.data["Inj Timing H Table (raw)"].rawData
        ign_prim = data_set.data["Primary Ign (�)"].rawData
        TGV_switch = data_set.data["TGV Map Ratio (mult)"].rawData
        #to find this we want to plot Time vs TPS/Req Torque
        t = data_set.data["Time (sec)"].rawData #get raw time data
        TPS = data_set.data["Throttle Pos (%)"].rawData
        #req_torque = data_set.Data["Req Torque (Nm)"].rawData
        fig, axs = plt.subplots(1,1)
        axs.plot(t,TPS, label = 'TPS')
        #axs.plot(t,req_torque, label = 'Req Torque')
        axs.set(xlabel = 'Time (s)')
        axs.legend()
        axs_right = axs.twinx()
        axs_right.plot(t,knock,label = 'Feedback Knock', color ='red')
        axs_right.legend()
        axs.set_title('Time Series')
        plt.show()

        while True:
            t1 = input('Please enter lower bound for window')
            t2 = input('Please enter upper bound for time window')
            if t2 > t1:
                break


        #reshape the time series
        locs = np.where(t > np.int(t1))
        t = t[locs]
        tps= TPS[locs]
        boost = boost[locs]
        target_boost = target_boost[locs]
        TD_error = TD_error[locs]
        knock = knock[locs]
        AFR= AFR[locs]
        comm_AFR = comm_AFR[locs]
        ign_timing = ign_timing[locs]
        inj_timing= inj_timing[locs]
        ign_prim = ign_prim[locs]
        rpm = rpm[locs]
        load = load[locs]
        MAF  = MAF[locs]
        TGV_switch = TGV_switch[locs]
        locs = np.where(t< np.int(t2))
        t = t[locs]
        tps= tps[locs]
        boost = boost[locs]
        target_boost = target_boost[locs]
        TD_error = TD_error[locs]
        knock = knock[locs]
        AFR= AFR[locs]
        comm_AFR = comm_AFR[locs]
        ign_timing = ign_timing[locs]
        inj_timing= inj_timing[locs]
        ign_prim = ign_prim[locs]
        rpm = rpm[locs]
        load = load[locs]
        MAF = MAF[locs]
        TGV_switch = TGV_switch[locs]
        #plt.clf()

        fig = plt.figure()
        ax = fig.add_subplot(2,2,1)
        ax.plot(t, boost, label ='Boost (psi)')
        ax.plot(t, target_boost, label = 'Target Boost (psi)')
        ax.plot(t, TD_error, label = 'TD boost error')
        ax.legend()
        ax.set_title('Boost at WOT')

        ax = fig.add_subplot(2,2,2)
        ax.plot(t,tps,label = 'TPS', color = 'black')
        axs_right = ax.twinx()
        axs_right.plot(t,knock, label = 'knock',color = 'red')
        ax.legend()
        axs_right.legend()
        ax.set_title('TPS and knock ')

        #ax=fig.add_subplot(2,2,3,projection = '3d')
        #ax.plot3D(t,rpm,load,label = 'RPM and Load')
        #ax.legend()
        ax = fig.add_subplot(2,2,3)
        ax.plot(t,rpm, label = 'RPM',color = 'black')
        ax.legend()
        ax_right = ax.twinx()
        ax_right.plot(t,load, label = 'Load',color = 'red')
        ax_right.legend()
        ax = fig.add_subplot(2,2,4)
        ax.plot(t,MAF, label = 'MAF', color = 'black')
        ax_right = ax.twinx()
        ax_right.plot(t,TGV_switch, label = 'TGV switch multiplier', color = 'red')
        ax_right.legend()
        ax.legend()
        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(2,2,1)
        ax.plot(t, AFR, label ='AFR')
        ax.plot(t, comm_AFR, label = 'comm AFR')
        ax.legend()
        ax.set_title('Boost at WOT')
        ax = fig.add_subplot(2,2,2)
        ax.plot(t,tps,label = 'TPS', color = 'black')
        axs_right = ax.twinx()
        axs_right.plot(t,knock, label = 'knock',color = 'red')
        ax.legend()
        axs_right.legend()
        ax.set_title('TPS and knock ')
        ax = fig.add_subplot(2,2,3)
        ax.plot(t,rpm, label = 'RPM',color = 'black')
        ax.legend()
        ax_right = ax.twinx()
        ax_right.plot(t,load, label = 'Load',color = 'red')
        ax_right.legend()
        ax = fig.add_subplot(2,2,4)
        ax.plot(t,MAF, label = 'MAF', color = 'black')
        ax_right = ax.twinx()
        ax_right.plot(t,TGV_switch, label = 'TGV switch multiplier', color = 'red')
        ax_right.legend()
        ax.legend()


        plt.show()

        fig = plt.figure()
        ax = fig.add_subplot(2,2,1)
        ax.plot(t, ign_timing, label ='Ignition timing')
        ax.plot(t, ign_prim, label = 'Primary ignition')
        axs_right_1 = ax.twinx()
        axs_right_1.plot(t,inj_timing, label = 'Injector timing',color = 'black')
        axs_right_1.legend()
        ax.legend()
        ax.set_title('Boost at WOT')
        ax = fig.add_subplot(2,2,2)
        ax.plot(t,tps,label = 'TPS', color = 'black')
        axs_right = ax.twinx()
        axs_right.plot(t,knock, label = 'knock',color = 'red')
        ax.legend()
        axs_right.legend()
        ax.set_title('TPS and knock ')
        ax = fig.add_subplot(2,2,3)
        ax.plot(t,rpm, label = 'RPM',color = 'black')
        ax.legend()
        ax_right = ax.twinx()
        ax_right.plot(t,load, label = 'Load',color = 'red')
        ax_right.legend()
        ax = fig.add_subplot(2,2,4)
        ax.plot(t,MAF, label = 'MAF', color = 'black')
        ax_right = ax.twinx()
        ax_right.plot(t,TGV_switch, label = 'TGV switch multiplier', color = 'red')
        ax_right.legend()
        ax.legend()

        plt.show()

        return_base()


def plot_VE_dist():
    with open('MAF_VE_corr_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_CL_MAF():
    with open('CL_MAF_offsets.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_MAF_corr():
    with open('MAF_corr.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_fuel_dist():
    with open('fuel_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_knock():
    with open('knock.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_fkl():
    with open('fkl_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_coolant_dist():
    with open('coolant_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_int_dist():
    with open('int_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_int_man_dist():
    with open('int_man_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_oil_temp_dist():
    with open('oil_temp_dist.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
def plot_temps():
    with open('temperature_time_series.pkl','rb') as fid:
        ax = pickle.load(fid)
    plt.show()
