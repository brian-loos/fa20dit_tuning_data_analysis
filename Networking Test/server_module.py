import socket
import numpy as np
import time
from matplotlib import pyplot as plt
import obd
import pickle

global command_table,BUFFER_SIZE
global monitors,locs,conn,s
command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100,'percent'],
            5: ['COOLANT_TEMP',True,-10,60,'Celcius'], 6: ['SHORT_FUEL_TRIM_1',True,0,100,'percent'], 7: ['LONG_FUEL_TRIM_1',True,0,100,'percent'],8: ['SHORT_FUEL_TRIM_2',True,0,100,'percent'],
            9: ['LONG_FUEL_TRIM_2',True,0,100,'percent'], 10: ['FUEL_PRESSURE',True,0,1000,'kpa'], 11:['INTAKE_PRESSURE',True,-100,300,'kpa'], 12: ['RPM',True,0,7000,'RPM'],
            13: ['SPEED',True,0,200,'kph'], 14: ['TIMING_ADVANCE',True,-5,25,'degrees'], 15: ['INTAKE_TEMP',True,-10,60,'Celcius'], 16:['MAF',True,0,500,'g/s'], 17: ['THROTTLE_POS',True,0,100,'percent'],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1,'Volts'], 21:[ 'O2_B1S2',True,0,1,'Volts'], 22: ['O2_B1S3',True,0,1,'Volts'],
            23: ['O2_B1S4',True,0,1,'Volts'], 24:['O2_B2S1',True,0,1,'Volts'], 25: ['O2_B2S2',True,0,1,'Volts'], 26: ['O2_B2S3',True,0,1,'Volts'], 27: ['O2_B2S4',True,0,1,'Volts'],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600,'seconds'], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000,'km'], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000,'kpa'],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000,'kpa'], 36:['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],37: ['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],
            38:['O2_S3_WR_VOLTAGE',True,0,1,'Volts'], 39: ['O2_S4_WR_VOLTAGE',True,0,1,'Volts'], 40: ['O2_S5_WR_VOLTAGE',True,0,1,'Volts'],
            41: ['O2_S6_WR_VOLTAGE',True,0,1,'Volts'], 42: ['O2_S7_WR_VOLTAGE',True,0,1,'Volts'], 43:['O2_S8_WR_VOLTAGE',True,0,1,'Volts'],
            44: ['COMMANDED_EGR',True,0,100,'percent'], 45: ['EGR_ERROR',True,0,100,'percent'], 46: ['EVAPORATIVE_PURGE',True,0,100,'percent'], 47: ['FUEL_LEVEL',True,0,100,'percent'],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100,'count'], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000,'km'], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000,'pa'],
            51: ['BAROMETRIC_PRESSURE',True,50,150,'kpa'], 52: ['O2_S1_WR_CURRENT',True,100,600,'milliamps'], 52: ['O2_S2_WR_CURRENT',True,100,600,'milliamps'],
            53: ['O2_S3_WR_CURRENT',True,100,600,'milliamps'], 54: ['O2_S4_WR_CURRENT',True,100,600,'milliamps'], 55: ['O2_S5_WR_CURRENT',True,100,600,'milliamps'], 56 :['O2_S6_WR_CURRENT',True,100,600,'milliamps'],
            57: ['O2_S7_WR_CURRENT',True,100,600,'milliamps'], 58: ['O2_S8_WR_CURRENT',True,100,600,'milliamps'], 59: ['CATALYST_TEMP_B1S1',True,-10,300,'milliamps'],
            60: ['CATALYST_TEMP_B1S2',True,-10,300,'milliamps'], 61: ['CATALYST_TEMP_B2S1',True,-10,300,'milliamps'], 62: ['CATALYST_TEMP_B2S2',True,-10,300,'milliamps'], 63: ['PIDS_C',False],
            64: ['STATUS_DRIVE_CYCLE',False], 65: ['CONTROL_MODULE_VOLTAGE',True,0,15,'Volt'], 66:[ 'ABSOLUTE_LOAD',True,0,100,'percent'],
            67: ['COMMANDED_EQUIV_RATIO',True,0,23,'ratio'], 68: ['RELATIVE_THROTTLE_POS',True,0,100,'percent'], 69: ['AMBIANT_AIR_TEMP',True,-10,60,'Celcius'],
            70: ['THROTTLE_POS_B',True,0,100,'percent'], 71: ['THROTTLE_POS_C',True,0,100,'percent'], 72: ['ACCELERATOR_POS_D',True,0,100,'percent'],
            73: ['ACCELERATOR_POS_E',True,0,100,'percent'], 74: ['ACCELERATOR_POS_F',True,0,100,'percent'], 75: ['THROTTLE_ACTUATOR',True,0,100,'percent'], 76: ['RUN_TIME_MIL',True,0,3600,'minutes'],
            77: ['TIME_SINCE_DTC_CLEARED',True,0,3600,'minutes'], 78: ['unsupported',False], 79: ['MAX_MAF',True,0,500,'g/s'], 80:['FUEL_TYPE',False],
            81: ['ETHANOL_PERCENT',True,0,100,'percent'], 82: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100,'kpa'], 83: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200,'pa'],
            84: ['SHORT_O2_TRIM_B1',True,0,100,'percent'], 85: ['LONG_O2_TRIM_B1',True,0,100,'percent'], 86: ['SHORT_O2_TRIM_B2',True,0,100,'percent'], 87: ['LONG_O2_TRIM_B2',True,0,100,'percent'],
            88: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000,'kpa'], 89:['RELATIVE_ACCEL_POS',True,0,100,'percent'], 90 : ['HYBRID_BATTERY_REMAINING',True,0,100,'percent'],
            91: ['OIL_TEMP',True,-10,200,'Celcius'], 92: ['FUEL_INJECT_TIMING',True,-5,25,'degrees'], 93:['FUEL_RATE',True,0,10,'L/hr'], 94: ['unsupported',False] }
#starting the application
#first we must decide which values PIDS we want, there is an identical table onboard, so this is mainly for reference
#connecting subroutine
def setup_connection(TCP_IP,TCP_PORT,buffer):
    global conn, s,BUFFER_SIZE
    #TCP_IP = '25.17.212.221'
    BUFFER_SIZE = buffer
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((str(TCP_IP),TCP_PORT))
    s.listen()
    conn, addr = s.accept()
    print('Connection Address: ', addr)
    conn.sendall(b'start')#tell other side to start

    return 'success'#return status
#want to check status of client after connecting, this will get the current state
def get_status_client():#we want to check this before every posible query:
    conn.sendall(b'status') #get status from the other side
    data = conn.recv(BUFFER_SIZE) #get the response
    return data #this will return the status of the client
#want to recieve and set up OBD stuff first
def get_active_PIDs():
    global locs,BUFFER_SIZE #need global access to this
    conn.sendall(b'get_PIDs')#tells the client to fetch PIDs from the car
    print('getting pids')
    sz_locs = conn.recv(BUFFER_SIZE) #get the data in the buffer
    conn.sendall(b'recieved')#don't need to wait for response
    sz_locs = np.frombuffer(sz_locs,dtype=np.int32)
    #print(sz_locs)
    locs = conn.recv(BUFFER_SIZE)
    conn.sendall(b'recieved')
    locs = np.frombuffer(locs,dtype=np.int32,count=sz_locs[0])
    print('the following are the supported PIDs to use on this vehicle')
    file = open('pids.txt','w')
    for x in locs: #for now this prints to the command line
        if x < 95:
            if command_table[x][1]:
                print(x,command_table[x][0])
                file.write(command_table[x][0]+'\n')

    file.close()
    return locs
#need to prompt the user for which PID values they would like to use
#def set_desired_monitors():#depricated
    global monitors #generates the list monitors
    monitors = []
    def RepresentsInt(s):
        try:
            int(s)
            return True
        except ValueError:
            return False
    #generate a list of monitors from user input
    while 1:
        print('currently tracking: ')
        for i in range(len(monitors)):
            print(monitors[i], command_table[monitors[i]][0])
        print('Please enter the value to track, enter no value to continue or i for list of monitors available:')
        val = input()
        if val == '':
            break
        if RepresentsInt(val):
            val = int(val)#ensure this is valid
            if (val <= 94) and (val >= 1):
                if val not in monitors:
                    monitors.append(val)
                else:
                    print('already tracking that PID value')
            else:
                print('please enter a number in 1-94')

        elif val == 'i':
            for x in locs[0]:
                if x < 95:
                    if command_table[x][1]:
                        print(x,command_table[x][0])
        else:
            print('Please enter a number')
#need to send the list of monitors over to the other computer
def send_PIDs(monitors): #send PIDs to client side
    conn.sendall(b'set_mon')
    monitors = np.array(monitors)
    conn.sendall(np.int32(np.size(monitors)).tobytes())
    conn.recv(BUFFER_SIZE)
    conn.sendall(monitors.tobytes())#sending an int array of unknown length
    print(conn.recv(BUFFER_SIZE))
#initialize a figure object based off of how many values we sent, 4 bar graphs seem like a safe maximum, 4 horizontal is overkill, so probably not
def init_fig():#must run this before main loop
    max_vert = 4
    max_hor = 4
    #want to figure out how to grid this, just fill vertically as much as possible, number of columns will be total #/max_vert
    if np.size(monitors) > max_vert*max_hor: # to  many variables to track
        print('error, too many parameters being tracked')
        conn.close() #end program


    n = int(np.ceil(np.size(monitors)/max_vert)) #this sort of works, should kind of work now...
    m = min(max_vert,np.size(monitors)) # will overshoot the number of subplots needed which is fine, just won't update all of them
    view_window = 10 #this is fairly resonable for now
    #need to make y-limit box based off varying different ranges which will be acceptalbe, I'll keep this for now
    #RPM is easiest to test, so make sure that bounds are big


    fig,axs = plt.subplots(m,n)
    lines = []
    for i in range(n):
        for j  in range(m):
            # we should be good if we can make it here
            if (j+i*m < np.size(monitors)):
                print(j+i*m)
                print(command_table[monitors[j+i*m]])
                ymin = command_table[monitors[j+i*m ]][2]
                ymax = command_table[monitors[j+i*m]][3]
                if n > 1:
                    plot, = axs[j,i].plot([], lw = 1)
                    lines.append(plot)
                    axs[j,i].set_xlim(-1*view_window,0)
                    axs[j,i].set_ylim(ymin,ymax)
                    axs[j,i].set_title(command_table[monitors[j+i*m]][0])
                    axs[j,i].set_xlabel('time')
                    axs[j,i].set_ylabel(command_table[monitors[j+i*m]][4])
                else:#1-d axis plots
                    plot, = axs[j].plot([], lw = 1)
                    lines.append(plot)
                    axs[j].set_xlim(-1*view_window,0)
                    axs[j].set_ylim(ymin,ymax)
                    axs[j].set_title(command_table[monitors[j+i*m]][0])
                    axs[j].set_xlabel('time')
                    axs[j].set_ylabel(command_table[monitors[j+i*m]][4])
    plt.get_current_fig_manager().window.state('zoomed')
    fig.canvas.draw()

    backgrounds = []
    for i in range(n):
        for j in range(m):
            if (j+i*m < np.size(monitors)):
                if n >1:
                    backgrounds.append(fig.canvas.copy_from_bbox(axs[j,i].bbox))#cache
                else:
                    backgrounds.append(fig.canvas.copy_from_bbox(axs[j].bbox))#cache


    plt.show(block = False) #don't block
def main_query():
    #need to tell other side to start sending set_data
    conn.sendall(b'query')#tell it to start querying
    start= 0.
    vals = np.array([])
    times = np.array([])
    count = 0
    last_time = 0
    num_vars = np.size(monitors)
    while 1:
        data = conn.recv(BUFFER_SIZE)
        conn.sendall(data)#send back to confirm v
        #t = time.time(), wantto get this from the client side,we don't care about local time
        local_t = time.time() #maybe care about local time
        #update values
        count += 1
        #now want to make sure that all all the values are
        temp = np.frombuffer(data,dtype=np.single)#get all the values
        vals = np.append(vals,temp[0:num_vars]).reshape(count,num_vars) #this is an expensive thing to keep on having to do
        if count == 1:
            start = temp[num_vars] #still might be weird but less weird

        t = temp[num_vars]#last value should be time
        if len(times) != 0:
            times = times - (t-start -last_time) #shift all values
        last_time = t-start #new adjusted time
        times = np.append(times, [0.])
        for i in range(n):
            for j in range(m):
                if(j+i*m  < num_vars+1):
                    lines[j+i*m].set_data(times,vals[:,j+i*m])
                    if max(times) > view_window:
                        if n > 1:
                            axs[j,i].set_xlim(max(times)-10,max(times))
                        else:
                            axs[j].set_xlim(max(times)-10,max(times))
                    else:
                        if n > 1:
                            axs[j,i].set_xlim(-1*view_window,0)
                        else:
                            axs[j].set_xlim(-1*view_window,0)
                    #axs[i,j].set_ylim(min(vals[:,i+j*m]),max(vals[:,i+j*m]))
                    fig.canvas.restore_region(backgrounds[j+i*m])
                    if n > 1:
                        axs[j,i].draw_artist(lines[j+i*m])
                        fig.canvas.blit(axs[j,i].bbox)
                    else:
                        axs[j].draw_artist(lines[j+i*m])
                        fig.canvas.blit(axs[j].bbox)



        #line.set_data(times,x)
        #line2.set_data(times,y)

        #if max(times)>10:
        #    ax.set_xlim(max(times)-10,max(times))#max of some range probably like 10?
        #    ax2.set_xlim(max(times)-10,max(times))
        #else:
        #    ax.set_xlim(0,max(times))
        #    ax2.set_xlim(0,max(times))

        #ax.set_ylim(min(x),max(x))
        #ax2.set_ylim(min(y),max(y))
        #fig.canvas.draw()
        fig.canvas.flush_events()

        if not data: break
        #print("recieved data: :", np.frombuffer(data,dtype=np.int64))
        #print(t-start)
def close_conn():
    conn.sendall(b'close_all')
    s.close()
def restart_conn():
    conn.sendall(b'restart')
    time.sleep(3)
    s.close()
    conn.close()
