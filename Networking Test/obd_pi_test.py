import socket
import pandas as pd
import numpy as np
import time
import obd

#establish an OBD connection
print('establishing OBD connection')
connection = obd.OBD(fast = False) #hopefully autoconnect will work in this instance
#connection = obd.Async()
#connection.watch(obd.commands.RPM)
#connection.start()
#print(connection.query(obd.commands.RPM))
#connection.stop()

#need to probe the OBD status of the car, somehow


while 1:
    try:
        if connection.is_connected(): break
    except:
        pass
    print('trying to connect to car, ensure ignition is on')

print('connection established')
#maybe probe here to check if the car supports certain functions that we are expecting
#if we are here then, we are connected to the car, I think
#mode 1 command table:
command_table = {0: 'PIDS_A', 1: 'Status', 2:'FREEZE_DTC', 3:'FUEL_STATUS', 4:'ENGINE_LOAD',
            5: 'COOLANT_TEMP', 6: 'SHORT_FUEL_TRIM_1', 7: 'LONG_FUEL_TRIM_1',8: 'SHORT_FUEL_TRIM_2',
            9: 'LONG_FUEL_TRIM_2', 10: 'FUEL_PRESSURE', 11:'INTAKE_PRESSURE', 12: 'RPM',
            13: 'SPEED', 14: 'TIMING_ADVANCE', 15: 'INTAKE_TEMP', 16:'MAF', 17: 'THROTTLE_POS',
            18: 'AIR_STATUS', 19:'O2_SENSORS', 20: 'O2_B1S1', 21: 'O2_B1S2', 22: 'O2_B1S3',
            23: 'O2_B1S4', 24:'O2_B2S1', 25: 'O2_B2S2', 26: 'O2_B2S3', 27: 'O2_B2S4',
            28: 'OBD_COMPLIANCE', 29: 'O2_SENSORS_ALT', 30: 'AUX_INPUT_STATUS',
            31: 'RUN_TIME', 32: 'PIDS_B', 33: 'DISTANCE_WITH_MIL', 34:'FUEL_RAIL_PRESSURE_VAC',
            35: 'FUEL_RAIL_PRESSURE_DIRECT', 36:'O2_S2_WR_VOLTAGE',37: 'O2_S2_WR_VOLTAGE',
            38:'O2_S3_WR_VOLTAGE', 39: 'O2_S4_WR_VOLTAGE', 40: 'O2_S5_WR_VOLTAGE',
            41: 'O2_S6_WR_VOLTAGE', 42: 'O2_S7_WR_VOLTAGE', 43:'O2_S8_WR_VOLTAGE',
            44: 'COMMANDED_EGR', 45: 'EGR_ERROR', 46: 'EVAPORATIVE_PURGE', 47: 'FUEL_LEVEL',
            48: 'WARMUPS_SINCE_DTC_CLEAR', 49: 'DISTANCE_SINCE_DTC_CLEAR', 50: 'EVAP_VAPOR_PRESSURE',
            51: 'BAROMETRIC_PRESSURE', 52: 'O2_S1_WR_CURRENT', 52: 'O2_S2_WR_CURRENT',
            53: 'O2_S3_WR_CURRENT', 54: 'O2_S4_WR_CURRENT', 55: 'O2_S5_WR_CURRENT', 56 :'O2_S6_WR_CURRENT',
            57: 'O2_S7_WR_CURRENT', 58: 'O2_S8_WR_CURRENT', 59: 'CATALYST_TEMP_B1S1',
            60: 'CATALYST_TEMP_B1S2', 61: 'CATALYST_TEMP_B2S1', 62: 'CATALYST_TEMP_B2S2', 63: 'PIDS_C',
            64: 'STATUS_DRIVE_CYCLE', 65: 'CONTROL_MODULE_VOLTAGE', 66: 'ABSOLUTE_LOAD',
            67: 'COMMANDED_EQUIV_RATIO', 68: 'RELATIVE_THROTTLE_POS', 69: 'AMBIANT_AIR_TEMP',
            70: 'THROTTLE_POS_B', 71: 'THROTTLE_POS_C', 72: 'ACCELERATOR_POS_D',
            73: 'ACCELERATOR_POS_E', 74: 'ACCELERATOR_POS_F', 75: 'THROTTLE_ACTUATOR', 76: 'RUN_TIME_MIL',
            77: 'TIME_SINCE_DTC_CLEARED', 78: 'unsupported', 79: 'MAX_MAF', 80:'FUEL_TYPE',
            81: 'ETHANOL_PERCENT', 82: 'EVAP_VAPOR_PRESSURE_ABS', 83: 'EVAP_VAPOR_PRESSURE_ALT',
            84: 'SHORT_O2_TRIM_B1', 85: 'LONG_O2_TRIM_B1', 86: 'SHORT_O2_TRIM_B2', 87: 'LONG_O2_TRIM_B2',
            88: 'FUEL_RAIL_PRESSURE_ABS', 89:'RELATIVE_ACCEL_POS', 90 : 'HYBRID_BATTERY_REMAINING',
            91: 'OIL_TEMP', 92: 'FUEL_INJECT_TIMING', 93:'FUEL_RATE', 94: 'unsupported' }
#connection.watch(obd.commands.PIDS_A)
#connection.watch(obd.commands.PIDS_B)
#connection.watch(obd.commands.PIDS_C)
#connection.start()
r1 = connection.query(obd.commands.PIDS_A)
r2 = connection.query(obd.commands.PIDS_B)
r3 = connection.query(obd.commands.PIDS_C)
#probably want to send over these bit arrays#hopefully this still works after changing to async
#convert them to logical arrays
#connection.stop()
if r1.value == None:
    r1.value = b'00000000000000000000000000000000'
if r2.value == None:
    r2.value = b'00000000000000000000000000000000'
if r3.value == None:
    r3.value = b'00000000000000000000000000000000'
locs = np.append(0,np.array(list(r1.value)))
locs = np.append(locs,0)
locs = np.append(locs, np.array(list(r2.value)))
locs = np.append(locs,0)
locs = np.append(locs, np.array(list(r3.value)))
locs = np.where(locs == 1)

locs = locs[0]
sz = np.size(locs)

for x in locs:
    if x < 94:
        print(command_table[x])#show avaiable commands
#need to make sure this is correct:
#basic RPM query
print('testing blocking timing')

t_last = time.time()
t_avg = 0.
avg = 0.

monitors = np.array([12,15])
#for monitor in monitors:
    #connection.watch(obd.commands[1][monitor])
#we already know that blocking is all that will work, just need to make sure that this will work in the car
count = 100
for i in range(count):
    t_now = time.time()
    avg += t_now - t_last
    #print(t_now - t_last)#this will give us a good idea of how long every iteration takes, we cna use this to test scalability
    for monitor in monitors:
        r = connection.query(obd.commands[1][monitor])
        if not r.is_null():
            print(np.single(r.value.magnitude))
        else:
            print('null value')
    t_last = t_now

    time.sleep(.005)
avg = avg/count
print('the average time to retrieve and print data to the screen was ',avg)

connection.close()
