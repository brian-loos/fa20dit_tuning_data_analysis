import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import socket, time, os, glob, pickle, obd, sys, server_module

global TCP_IP, TCP_PORT, BUFFER_SIZE
global command_table,monitors,comm,max_selected_mons,max_cols,max_rows

class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def run(self):
        """Long-running task."""
        cwd = os.getcwd()
        os.chdir(cwd)
        file = open('data_to_send.csv','wb')#send data over with pickle
        pickle.dump([max_cols,max_rows,TCP_IP,TCP_PORT,BUFFER_SIZE,monitors],file)
        file.close()
        server_module.restart_conn()
        os.system('del stop.txt')#attempt to delete
        os.system('start /wait cmd /c python main_backend.py')
        #os.system('python main_backend.py')
class dropdown_widget(QWidget):
    global max_cols,max_rows
    def __init__(self,name , Items):
        super().__init__()
        self.selection = ''
        layout = QGridLayout() #this is the layout for the widget
        label = QLabel(name)
        label.setFont(QFont('Lucidia Console',16))
        self.dropdown_menu = QComboBox()
        self.dropdown_menu.addItems(Items)
        self.dropdown_menu.currentIndexChanged.connect(self.selection_changed)
        self.name = name
        layout.addWidget(label,0,0)
        layout.addWidget(self.dropdown_menu,0,1)
        self.setLayout(layout)

    def selection_changed(self,q):
        global max_cols,max_rows,max_selected_mons
        self.selection = self.dropdown_menu.currentText()
        if self.name == 'Set Max Rows':
            max_rows = int(self.selection)
        elif self.name == 'Set Max Cols':
            max_cols = int(self.selection)
        max_selected_mons = max_cols*max_rows


class pids_selection(QWidget):
    def __init__(self):
        super().__init__()
        self.layout = QVBoxLayout()
        #need to create a checkbox for each PID value
        self.boxes = []
        self.locs = []
        i = 0
        for item in command_table.values():
            if item[1] == True:
                name = item[0]#the name of the PIDs
                temp = QCheckBox(name)
                temp.setChecked(False)
                temp.setDisabled(True)
                self.boxes.append(temp)
                self.layout.addWidget(self.boxes[i],i)
                i += 1

        self.setLayout(self.layout)

    def update_pids(self,locs):
        for box in self.boxes:
            self.layout.removeWidget(box)
            box.deleteLater()
            box = None
        self.boxes = []
        i=-1
        self.locs = locs
        for loc in locs:
            item = command_table[loc]
            name = item[0]
            if item[1] == True:
                i+=1
                temp = QCheckBox(name)
                temp.setChecked(False)
                self.boxes.append(temp)
                self.boxes[i].stateChanged.connect(lambda checked, a = i:self.chkstate(self.boxes[a]))
                self.layout.addWidget(self.boxes[i],i)


    def chkstate(self,b):
        for loc in self.locs:
            item = command_table[loc]
            name = item[0]
            if name == b.text():
                if b.isChecked():
                    #need to check if we have too many
                    if len(monitors) < max_selected_mons:
                        monitors.append(loc)#append the current location to monitors list
                    else:
                        b.setChecked(False)#do not let check
                        msg = QMessageBox()
                        msg.setText("Too many monitors selected, please increase number of rows or columns")
                        msg.setWindowTitle("Warning")
                        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)

                elif (loc in monitors):
                    monitors.remove(loc)
                print(b.text())
                print(monitors)

class Window(QMainWindow):

    def __init__(self,parent = None):#marks as main window
        super().__init__(parent)
        #any other setup
        self.locs=[]
        self.setup_fname = "setup.csv"
        self.red_icon = "icons\\led_red.jpg"
        self.green_icon = "icons\\led_green.jpg"
        if len(glob.glob(self.setup_fname)) > 0:#finds a setup csv file
            self.setup_params()#sets up the parameters
        else:
            self.TCP_IP = None
            self.TCP_PORT = None
            self.BUFFER_SIZE = None
        sizeObject = QDesktopWidget().screenGeometry(-1)
        self.setupUi()

    def setup_params(self):
        global TCP_IP,TCP_PORT,BUFFER_SIZE
        params = pd.DataFrame(pd.read_csv(self.setup_fname, sep = ',', header = 0))
        self.TCP_IP = params['TCP IP Target'][0]
        self.TCP_PORT = params['TCP PORT Target'][0]
        self.BUFFER_SIZE = params['TCP BUFFER SIZE'][0]
        TCP_IP = self.TCP_IP
        TCP_PORT = self.TCP_PORT
        BUFFER_SIZE = self.BUFFER_SIZE

    def setupUi(self):
        self.setWindowTitle('Realtime Plotting Telemetry')
        self.resize(800,600)
        self.move(100,100)
        self.title =  QLabel('<h1>Realtime Plotting Project<h1>', parent = self)
        self.title.setGeometry(50,20, 300,50)

        #needed to create a subwidget
        wid = QWidget(self) #create central widget
        self.setCentralWidget(wid)
        layout = QGridLayout()#create the grid layout
        wid.setLayout(layout)#this layout is the layout for our program
        #add widgets to this
        if True: #all of the widgets
            curr_state =QLabel('current target IP:     '+str(self.TCP_IP)+'\ncurrent Target port: '+str(self.TCP_PORT)
                                    +'\ncurrent buffer size:  '+str(self.BUFFER_SIZE),parent = self)
            #curr_state.setGeometry(50,60,400,70)
            curr_state.setFont(QFont('Lucidia Console',14))

            #self.IP_set.clicked.connect(self.run_main)#this should poll to setup the connection
            connect_to_client= QPushButton()
            connect_to_client.setText('Connect to client side')
            #connect_to_client.setGeometry(50,150,250,50)
            connect_to_client.setFont(QFont('Lucidia Console',16))
            connect_to_client.clicked.connect(self.connect2client)
            if self.TCP_IP == None:
                connect_to_client.setEnabled(False)#by default

            get_PIDs = QPushButton()
            get_PIDs.setText('Get Supported PIDs')
            #self.get_PIDs.setGeometry(50,225,250,50)
            get_PIDs.setFont(QFont('Lucidia Console',16))
            get_PIDs.clicked.connect(self.get_supported_pids)

            stop = QPushButton()
            stop.setText('stop plotting')
            #self.stop.setGeometry(50,300,250,50)
            stop.setFont(QFont('Lucidia Console', 16))
            stop.clicked.connect(self.set_stop)

            show_mons = QPushButton()
            show_mons.setText('Show selected monitors')
            show_mons.setFont(QFont('Lucidia Console', 16))
            show_mons.clicked.connect(self.show_selected_mons)

            self.start_plotting = QPushButton()
            self.start_plotting.setText('Start Plotting')
            self.start_plotting.setFont(QFont('Lucidia Console', 16))
            self.start_plotting.clicked.connect(self.run_main)

            sub_widg = QWidget()
            hbox = QHBoxLayout()
            sub_widg.setLayout(hbox)

            row_set = dropdown_widget(name ='Set Max Rows',Items = ['2','3','4','5'])
            col_set = dropdown_widget(name = 'Set Max Cols', Items = ['2','3','4','5'])


            pids_scroll = QScrollArea()
            self.checkable_pids = pids_selection()
            pids_scroll.setWidget(self.checkable_pids)
            pids_scroll.setWidgetResizable(True)
            pids_scroll.setFixedHeight(250)


            hbox.addWidget(row_set,0)
            hbox.addWidget(col_set,1)

            exit = QPushButton()
            exit.setText('close all')
            #exit.setGeometry(50,375,250,50)
            exit.setFont(QFont('Lucidia Console',16))
            exit.clicked.connect(self.close_all)



            layout.addWidget(exit,1,1)
            layout.addWidget(curr_state,0,0)
            layout.addWidget(stop,3,0)
            layout.addWidget(get_PIDs,2,0)
            layout.addWidget(connect_to_client,1,0)
            layout.addWidget(sub_widg,2,1)
            layout.addWidget(pids_scroll,3,1)
            layout.addWidget(show_mons,4,0)
            layout.addWidget(self.start_plotting,5,0)



        #menu bar stuff
        self.load_setup_action = QAction("&Open Setup Csv",self)
        self.load_setup_action.setShortcut("Ctrl+O")
        self.load_setup_action.setStatusTip('Load setup csv file')
        self.load_setup_action.triggered.connect(self.open_csv_finder)
        self.set_ip_menu = QAction("&Set Ip Manually",self)
        self.set_ip_menu.setShortcut("Ctrl+I")
        self.set_ip_menu.setStatusTip("set IP manually")
        self.set_ip_menu.triggered.connect(self.set_ip)

        menuBar = self.menuBar()
        fileMenu = menuBar.addMenu("&File")
        fileMenu.addAction(self.load_setup_action)
        editMenu = menuBar.addMenu("&Edit")
        networkMenu = menuBar.addMenu("&Network")
        networkMenu.addAction(self.set_ip_menu)
        helpMenu = menuBar.addMenu("&Help")

        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)
        self.statusBar.showMessage('Not Connected')



    def set_ip(self):
        text, ok = QInputDialog.getText(self, 'IP Input Dialog', 'Enter your IP Address:')
        if ok:#no checking if IP is valid as this point in time
            self.curr_state.setText('current target IP:'+str(text)+'\ncurrent Target port:'+str(self.TCP_PORT)
                                    +'\ncurrent buffer size:'+str(self.BUFFER_SIZE))
            self.connect_to_client.setEnabled(True)

    def set_stop(self):
        self.running = False
        #want to create a text file to say to stop the program
        file = open('stop.txt','w')
        file.write('stop')
        file.close()
        self.start_plotting.setEnabled(True)

    def close_all(self):
        server_module.close_conn()

    def connect2client(self):#method to connect to client
        self.statusBar.showMessage('connecting')
        try:
            server_module.setup_connection(self.TCP_IP,self.TCP_PORT,self.BUFFER_SIZE)
            self.statusBar.showMessage('Connected')
        except:
            self.statusBar.showMessage('Not Connected')

    def run_main(self):#need to use this threaded version
        #need to set maxcols,maxrows, monitors
        #sending PIDs to the client
        #start the main program
        self.thread = QThread()
        self.worker = Worker()
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        #self.worker.progress.connect(self.reportProgress)
        self.thread.start()
        self.start_plotting.setEnabled(False)
        self.thread.finished.connect(lambda: self.start_plotting.setEnabled(True))

    def get_supported_pids(self):#this will fetch the current available pids
        self.locs = server_module.get_active_PIDs()#gets the locs
        #need to update the locs in the widget
        print(self.locs)
        for i in range(len(self.locs)):
            if self.locs[i] > 95:
                self.locs = self.locs[0:i-1]
                break
        self.checkable_pids.update_pids(locs = self.locs)
        print('got locs')
        msg = QMessageBox()
        msg.setWindowTitle('Supported PIDs')
        file = open('pids.txt','r')
        pids = ""
        for lines in file:
            temp = file.read()
            pids +=temp   #these are the
        msg.setText(str(pids))
        msg.exec()

    def open_csv_finder(self):
        cwd = os.getcwd()
        fname = QFileDialog.getOpenFileName(self, 'Open file',
            cwd,"Csv files(*.csv)")#need to set the setup file
        #self.le.setPixmap(QPixmap(fname))
        try:
            self.setup_fname = os.path.relpath(fname[0],cwd) #this is the entire filepath
            self.setup_params()#reset parameters and update test
            self.curr_state.setText('current target IP:'+str(self.TCP_IP)+'\ncurrent Target port:'+str(self.TCP_PORT)
                                    +'\ncurrent buffer size:'+str(self.BUFFER_SIZE))
        except:
            print('no valid file chosen')

    def selection_plots_changed(self):
        print(self.configure_plots.currentText())

    def show_selected_mons(self):
        print(monitors)

command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100,'percent'],
            5: ['COOLANT_TEMP',True,-10,60,'Celcius'], 6: ['SHORT_FUEL_TRIM_1',True,0,100,'percent'], 7: ['LONG_FUEL_TRIM_1',True,0,100,'percent'],8: ['SHORT_FUEL_TRIM_2',True,0,100,'percent'],
            9: ['LONG_FUEL_TRIM_2',True,0,100,'percent'], 10: ['FUEL_PRESSURE',True,0,1000,'kpa'], 11:['INTAKE_PRESSURE',True,-100,300,'kpa'], 12: ['RPM',True,0,7000,'RPM'],
            13: ['SPEED',True,0,200,'kph'], 14: ['TIMING_ADVANCE',True,-5,25,'degrees'], 15: ['INTAKE_TEMP',True,-10,60,'Celcius'], 16:['MAF',True,0,500,'g/s'], 17: ['THROTTLE_POS',True,0,100,'percent'],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1,'Volts'], 21:[ 'O2_B1S2',True,0,1,'Volts'], 22: ['O2_B1S3',True,0,1,'Volts'],
            23: ['O2_B1S4',True,0,1,'Volts'], 24:['O2_B2S1',True,0,1,'Volts'], 25: ['O2_B2S2',True,0,1,'Volts'], 26: ['O2_B2S3',True,0,1,'Volts'], 27: ['O2_B2S4',True,0,1,'Volts'],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600,'seconds'], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000,'km'], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000,'kpa'],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000,'kpa'], 36:['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],37: ['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],
            38:['O2_S3_WR_VOLTAGE',True,0,1,'Volts'], 39: ['O2_S4_WR_VOLTAGE',True,0,1,'Volts'], 40: ['O2_S5_WR_VOLTAGE',True,0,1,'Volts'],
            41: ['O2_S6_WR_VOLTAGE',True,0,1,'Volts'], 42: ['O2_S7_WR_VOLTAGE',True,0,1,'Volts'], 43:['O2_S8_WR_VOLTAGE',True,0,1,'Volts'],
            44: ['COMMANDED_EGR',True,0,100,'percent'], 45: ['EGR_ERROR',True,0,100,'percent'], 46: ['EVAPORATIVE_PURGE',True,0,100,'percent'], 47: ['FUEL_LEVEL',True,0,100,'percent'],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100,'count'], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000,'km'], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000,'pa'],
            51: ['BAROMETRIC_PRESSURE',True,50,150,'kpa'], 52: ['O2_S1_WR_CURRENT',True,100,600,'milliamps'], 52: ['O2_S2_WR_CURRENT',True,100,600,'milliamps'],
            53: ['O2_S3_WR_CURRENT',True,100,600,'milliamps'], 54: ['O2_S4_WR_CURRENT',True,100,600,'milliamps'], 55: ['O2_S5_WR_CURRENT',True,100,600,'milliamps'], 56 :['O2_S6_WR_CURRENT',True,100,600,'milliamps'],
            57: ['O2_S7_WR_CURRENT',True,100,600,'milliamps'], 58: ['O2_S8_WR_CURRENT',True,100,600,'milliamps'], 59: ['CATALYST_TEMP_B1S1',True,-10,300,'milliamps'],
            60: ['CATALYST_TEMP_B1S2',True,-10,300,'milliamps'], 61: ['CATALYST_TEMP_B2S1',True,-10,300,'milliamps'], 62: ['CATALYST_TEMP_B2S2',True,-10,300,'milliamps'], 63: ['PIDS_C',False],
            64: ['STATUS_DRIVE_CYCLE',False], 65: ['CONTROL_MODULE_VOLTAGE',True,0,15,'Volt'], 66:[ 'ABSOLUTE_LOAD',True,0,100,'percent'],
            67: ['COMMANDED_EQUIV_RATIO',True,0,23,'ratio'], 68: ['RELATIVE_THROTTLE_POS',True,0,100,'percent'], 69: ['AMBIANT_AIR_TEMP',True,-10,60,'Celcius'],
            70: ['THROTTLE_POS_B',True,0,100,'percent'], 71: ['THROTTLE_POS_C',True,0,100,'percent'], 72: ['ACCELERATOR_POS_D',True,0,100,'percent'],
            73: ['ACCELERATOR_POS_E',True,0,100,'percent'], 74: ['ACCELERATOR_POS_F',True,0,100,'percent'], 75: ['THROTTLE_ACTUATOR',True,0,100,'percent'], 76: ['RUN_TIME_MIL',True,0,3600,'minutes'],
            77: ['TIME_SINCE_DTC_CLEARED',True,0,3600,'minutes'], 78: ['unsupported',False], 79: ['MAX_MAF',True,0,500,'g/s'], 80:['FUEL_TYPE',False],
            81: ['ETHANOL_PERCENT',True,0,100,'percent'], 82: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100,'kpa'], 83: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200,'pa'],
            84: ['SHORT_O2_TRIM_B1',True,0,100,'percent'], 85: ['LONG_O2_TRIM_B1',True,0,100,'percent'], 86: ['SHORT_O2_TRIM_B2',True,0,100,'percent'], 87: ['LONG_O2_TRIM_B2',True,0,100,'percent'],
            88: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000,'kpa'], 89:['RELATIVE_ACCEL_POS',True,0,100,'percent'], 90 : ['HYBRID_BATTERY_REMAINING',True,0,100,'percent'],
            91: ['OIL_TEMP',True,-10,200,'Celcius'], 92: ['FUEL_INJECT_TIMING',True,-5,25,'degrees'], 93:['FUEL_RATE',True,0,10,'L/hr'], 94: ['unsupported',False] }
monitors = []
max_rows = 2
max_cols = 2
max_selected_mons = 4
cwd=os.getcwd()
os.chdir(cwd)
os.system('del *.txt')


#to start the application
app = QApplication(sys.argv)
win = Window()
win.show()
sys.exit(app.exec())
