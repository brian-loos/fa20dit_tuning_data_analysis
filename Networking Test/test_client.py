import socket
import pandas as pd
import numpy as np
import time
import obd

#this is a test client to spoof the connection to obd
#establish an OBD connection
print('attempting to connect')
time.sleep(1)
#connection = obd.OBD() #hopefully autoconnect will work in this instance
#connection = obd.Async(fast = False)

#need to probe the OBD status of the car, somehow
#while 1:
#    try:
#        if connection.is_connected(): break
#    except:
#        pass
#    print('trying to connect to car, ensure ignition is on')
print('connection established')
#maybe probe here to check if the car supports certain functions that we are expecting
#if we are here then, we are connected to the car, I think
#mode 1 command table:
command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100],
            5: ['COOLANT_TEMP',True,-10,60], 6: ['SHORT_FUEL_TRIM_1',True,0,100], 7: ['LONG_FUEL_TRIM_1',True,0,100],8: ['SHORT_FUEL_TRIM_2',True,0,100],
            9: ['LONG_FUEL_TRIM_2',True,0,100], 10: ['FUEL_PRESSURE',True,0,1000], 11:['INTAKE_PRESSURE',True,-100,300], 12: ['RPM',True,0,7000],
            13: ['SPEED',True,0,100], 14: ['TIMING_ADVANCE',True,-5,25], 15: ['INTAKE_TEMP',True,-10,60], 16:['MAF',True,0,500], 17: ['THROTTLE_POS',True,0,100],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1], 21:[ 'O2_B1S2',True,0,1], 22: ['O2_B1S3',True,0,1],
            23: ['O2_B1S4',True,0,1], 24:['O2_B2S1',True,0,1], 25: ['O2_B2S2',True,0,1], 26: ['O2_B2S3',True,0,1], 27: ['O2_B2S4',True,0,1],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000], 36:['O2_S2_WR_VOLTAGE',True,0,1],37: ['O2_S2_WR_VOLTAGE',True,0,1],
            38:['O2_S3_WR_VOLTAGE',True,0,1], 39: ['O2_S4_WR_VOLTAGE',True,0,1], 40: ['O2_S5_WR_VOLTAGE',True,0,1],
            41: ['O2_S6_WR_VOLTAGE',True,0,1], 42: ['O2_S7_WR_VOLTAGE',True,0,1], 43:['O2_S8_WR_VOLTAGE',True,0,1],
            44: ['COMMANDED_EGR',True,0,100], 45: ['EGR_ERROR',True,0,100], 46: ['EVAPORATIVE_PURGE',True,0,100], 47: ['FUEL_LEVEL',True,0,100],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000],
            51: ['BAROMETRIC_PRESSURE',True,50,150], 52: ['O2_S1_WR_CURRENT',True,100,600], 52: ['O2_S2_WR_CURRENT',True,100,600],
            53: ['O2_S3_WR_CURRENT',True,100,600], 54: ['O2_S4_WR_CURRENT',True,100,600], 55: ['O2_S5_WR_CURRENT',True,100,600], 56 :['O2_S6_WR_CURRENT',True,100,600],
            57: ['O2_S7_WR_CURRENT',True,100,600], 58: ['O2_S8_WR_CURRENT',True,100,600], 59: ['CATALYST_TEMP_B1S1',True,-10,300],
            60: ['CATALYST_TEMP_B1S2',True,-10,300], 61: ['CATALYST_TEMP_B2S1',True,-10,300], 62: ['CATALYST_TEMP_B2S2',True,-10,300], 63: ['PIDS_C',False],
            64: ['STATUS_DRIVE_CYCLE',False], 65: ['CONTROL_MODULE_VOLTAGE',True,0,15], 66:[ 'ABSOLUTE_LOAD',True,0,100],
            67: ['COMMANDED_EQUIV_RATIO',True,0,23], 68: ['RELATIVE_THROTTLE_POS',True,0,100], 69: ['AMBIANT_AIR_TEMP',True,-10,60],
            70: ['THROTTLE_POS_B',True,0,100], 71: ['THROTTLE_POS_C',True,0,100], 72: ['ACCELERATOR_POS_D',True,0,100],
            73: ['ACCELERATOR_POS_E',True,0,100], 74: ['ACCELERATOR_POS_F',True,0,100], 75: ['THROTTLE_ACTUATOR',True,0,100], 76: ['RUN_TIME_MIL',True,0,3600],
            77: ['TIME_SINCE_DTC_CLEARED',True,0,3600], 78: ['unsupported',False], 79: ['MAX_MAF',True,0,500], 80:['FUEL_TYPE',False],
            81: ['ETHANOL_PERCENT',True,0,100], 82: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100], 83: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200],
            84: ['SHORT_O2_TRIM_B1',True,0,100], 85: ['LONG_O2_TRIM_B1',True,0,100], 86: ['SHORT_O2_TRIM_B2',True,0,100], 87: ['LONG_O2_TRIM_B2',True,0,100],
            88: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000], 89:['RELATIVE_ACCEL_POS',True,0,100], 90 : ['HYBRID_BATTERY_REMAINING',True,0,100],
            91: ['OIL_TEMP',True,-10,200], 92: ['FUEL_INJECT_TIMING',True,-5,25], 93:['FUEL_RATE',True,0,10], 94: ['unsupported',False] }
#establish a TCP connection
TCP_IP = '25.17.217.184'
TCP_PORT = 5005
BUFFER_SIZE = 640
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.connect((TCP_IP,TCP_PORT))

#set up communication, send over available PIDs
#send and wait

#example values, from HONDA PILOT
#r1 = connection.query(obd.commands.PIDS_A)
#r2 = connection.query(obd.commands.PIDS_B)
#r3 = connection.query(obd.commands.PIDS_C)
#print('getting available PIDS')
#if r1.value == None:
#    r1.value = b'00000000000000000000000000000000'
#if r2.value == None:
#    r2.value = b'00000000000000000000000000000000'
#if r3.value == None:
#    r3.value = b'00000000000000000000000000000000'
r1 =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
r2 =[0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,0,0,0,0,0]
r3 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]

locs = np.append(0,np.array(list(r1)))
locs = np.append(locs,0)
locs = np.append(locs, np.array(list(r2)))
locs = np.append(locs,0)
locs = np.append(locs, np.array(list(r3)))
locs = np.where(locs == 1)

locs = locs[0]
sz = np.int64(np.size(locs))
s.sendall(sz.tobytes())
print(s.recv(BUFFER_SIZE))
print('sending more shit')
s.sendall(locs.tobytes())
print(locs)
print(s.recv(BUFFER_SIZE))

#should recieve more data
length_mon =s.recv(BUFFER_SIZE)
s.sendall(b'recieved')
length_mon = (np.frombuffer(length_mon,dtype = np.int64))
print(length_mon[0])
monitors = s.recv(BUFFER_SIZE)
s.sendall(b'recieved')
monitors = (np.frombuffer(monitors, dtype=np.int32, count = length_mon[0]))
#assuming an Async table we will have to do the following:
#connection.close()
#after this step we are going to reset the TCP connection to connect to another python instance
while 1:
    data = s.recv(BUFFER_SIZE)
    print(data)
    if data == b'close': #want to close and restart the connection
        s.close()
        break
    time.sleep(1)

time.sleep(5)
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
#need to try this
while 1:
    try:
        s.connect((TCP_IP,TCP_PORT))
        break
    except:
        print('could not connect')


time.sleep(1)
#connection = obd.Async(fast=False)


#for monitor in monitors:
    #need to watch each one
#    connection.watch(obd.commands[1][monitor])#this should work

#connection.start()
prev_vals = np.zeros(np.size(monitors))
while 1:
    #print('eneter value to send: ')
    #val = input()
    #print('sending ',val)
    #val = np.int64(val)
    #generate random values
    #val = np.random.randint(-10,10,(10,1),dtype = np.int64) #random vector
    queue = []
    i = 0

    for monitor in monitors: #need to update values
        r = np.random.randint(0,100)#generate some random data
        queue.append(np.float(r))
        #r = connection.query(obd.commands[1][monitor])
        #if not r.is_null():
        #    queue.append(np.double(r.value.magnitude))#query the value
        #else:
        #    queue.append(prev_vals[i])#most recently seend value
        #i += 1
    #queue should be populated now
    queue = np.array(queue)
    prev_vals = queue
    t  = time.time() #get current time
    queue = np.append(queue, np.float(t))
    #now last value is time
    print(queue)
    s.sendall(queue.tobytes())
    data = s.recv(BUFFER_SIZE)
    if data == b'end':
        break
    time.sleep(.005)
s.close()
#connection.stop()
#connection.close()
