import socket
import numpy as np
import time

TCP_IP = '25.17.212.221'
TCP_PORT = 5005
BUFFER_SIZE = 640
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

s.connect((TCP_IP,TCP_PORT))
t1 = 0.
t2 = 0.
avg = 0.
count = 1000
for i in range(count):
    t1 = time.time()
    r = np.random.randint(0,100)
    s.sendall(np.array(r).tobytes())
    t2 = time.time()
    #print(t2-t1)
    avg += t2-t1

print(avg/count)
