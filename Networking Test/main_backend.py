import socket
import numpy as np
import time
from matplotlib import pyplot as plt
import obd
import pickle
import server_module
import glob
global command_table,BUFFER_SIZE
global monitors,locs,conn,s
command_table = {0: ['PIDS_A',False], 1: ['Status',False], 2:['FREEZE_DTC',False], 3:['FUEL_STATUS',False], 4:['ENGINE_LOAD',True,0,100,'percent'],
            5: ['COOLANT_TEMP',True,-10,60,'Celcius'], 6: ['SHORT_FUEL_TRIM_1',True,0,100,'percent'], 7: ['LONG_FUEL_TRIM_1',True,0,100,'percent'],8: ['SHORT_FUEL_TRIM_2',True,0,100,'percent'],
            9: ['LONG_FUEL_TRIM_2',True,0,100,'percent'], 10: ['FUEL_PRESSURE',True,0,1000,'kpa'], 11:['INTAKE_PRESSURE',True,-100,300,'kpa'], 12: ['RPM',True,0,7000,'RPM'],
            13: ['SPEED',True,0,200,'kph'], 14: ['TIMING_ADVANCE',True,-5,25,'degrees'], 15: ['INTAKE_TEMP',True,-10,60,'Celcius'], 16:['MAF',True,0,500,'g/s'], 17: ['THROTTLE_POS',True,0,100,'percent'],
            18: ['AIR_STATUS',False], 19:['O2_SENSORS',False], 20: ['O2_B1S1',True,0,1,'Volts'], 21:[ 'O2_B1S2',True,0,1,'Volts'], 22: ['O2_B1S3',True,0,1,'Volts'],
            23: ['O2_B1S4',True,0,1,'Volts'], 24:['O2_B2S1',True,0,1,'Volts'], 25: ['O2_B2S2',True,0,1,'Volts'], 26: ['O2_B2S3',True,0,1,'Volts'], 27: ['O2_B2S4',True,0,1,'Volts'],
            28: ['OBD_COMPLIANCE',False], 29: ['O2_SENSORS_ALT',False], 30: ['AUX_INPUT_STATUS',False],
            31: ['RUN_TIME',True,0,3600,'seconds'], 32: ['PIDS_B',False], 33: ['DISTANCE_WITH_MIL',True,0,10000,'km'], 34:['FUEL_RAIL_PRESSURE_VAC',True,0,1000,'kpa'],
            35: ['FUEL_RAIL_PRESSURE_DIRECT',True,0,1000,'kpa'], 36:['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],37: ['O2_S2_WR_VOLTAGE',True,0,1,'Volts'],
            38:['O2_S3_WR_VOLTAGE',True,0,1,'Volts'], 39: ['O2_S4_WR_VOLTAGE',True,0,1,'Volts'], 40: ['O2_S5_WR_VOLTAGE',True,0,1,'Volts'],
            41: ['O2_S6_WR_VOLTAGE',True,0,1,'Volts'], 42: ['O2_S7_WR_VOLTAGE',True,0,1,'Volts'], 43:['O2_S8_WR_VOLTAGE',True,0,1,'Volts'],
            44: ['COMMANDED_EGR',True,0,100,'percent'], 45: ['EGR_ERROR',True,0,100,'percent'], 46: ['EVAPORATIVE_PURGE',True,0,100,'percent'], 47: ['FUEL_LEVEL',True,0,100,'percent'],
            48: ['WARMUPS_SINCE_DTC_CLEAR',True,0,100,'count'], 49: ['DISTANCE_SINCE_DTC_CLEAR',True,0,10000,'km'], 50: ['EVAP_VAPOR_PRESSURE',True,-1000,1000,'pa'],
            51: ['BAROMETRIC_PRESSURE',True,50,150,'kpa'], 52: ['O2_S1_WR_CURRENT',True,100,600,'milliamps'], 52: ['O2_S2_WR_CURRENT',True,100,600,'milliamps'],
            53: ['O2_S3_WR_CURRENT',True,100,600,'milliamps'], 54: ['O2_S4_WR_CURRENT',True,100,600,'milliamps'], 55: ['O2_S5_WR_CURRENT',True,100,600,'milliamps'], 56 :['O2_S6_WR_CURRENT',True,100,600,'milliamps'],
            57: ['O2_S7_WR_CURRENT',True,100,600,'milliamps'], 58: ['O2_S8_WR_CURRENT',True,100,600,'milliamps'], 59: ['CATALYST_TEMP_B1S1',True,-10,300,'milliamps'],
            60: ['CATALYST_TEMP_B1S2',True,-10,300,'milliamps'], 61: ['CATALYST_TEMP_B2S1',True,-10,300,'milliamps'], 62: ['CATALYST_TEMP_B2S2',True,-10,300,'milliamps'], 63: ['PIDS_C',False],
            64: ['STATUS_DRIVE_CYCLE',False], 65: ['CONTROL_MODULE_VOLTAGE',True,0,15,'Volt'], 66:[ 'ABSOLUTE_LOAD',True,0,100,'percent'],
            67: ['COMMANDED_EQUIV_RATIO',True,0,23,'ratio'], 68: ['RELATIVE_THROTTLE_POS',True,0,100,'percent'], 69: ['AMBIANT_AIR_TEMP',True,-10,60,'Celcius'],
            70: ['THROTTLE_POS_B',True,0,100,'percent'], 71: ['THROTTLE_POS_C',True,0,100,'percent'], 72: ['ACCELERATOR_POS_D',True,0,100,'percent'],
            73: ['ACCELERATOR_POS_E',True,0,100,'percent'], 74: ['ACCELERATOR_POS_F',True,0,100,'percent'], 75: ['THROTTLE_ACTUATOR',True,0,100,'percent'], 76: ['RUN_TIME_MIL',True,0,3600,'minutes'],
            77: ['TIME_SINCE_DTC_CLEARED',True,0,3600,'minutes'], 78: ['unsupported',False], 79: ['MAX_MAF',True,0,500,'g/s'], 80:['FUEL_TYPE',False],
            81: ['ETHANOL_PERCENT',True,0,100,'percent'], 82: ['EVAP_VAPOR_PRESSURE_ABS',True,0,100,'kpa'], 83: ['EVAP_VAPOR_PRESSURE_ALT',True,0,200,'pa'],
            84: ['SHORT_O2_TRIM_B1',True,0,100,'percent'], 85: ['LONG_O2_TRIM_B1',True,0,100,'percent'], 86: ['SHORT_O2_TRIM_B2',True,0,100,'percent'], 87: ['LONG_O2_TRIM_B2',True,0,100,'percent'],
            88: ['FUEL_RAIL_PRESSURE_ABS',True,0,1000,'kpa'], 89:['RELATIVE_ACCEL_POS',True,0,100,'percent'], 90 : ['HYBRID_BATTERY_REMAINING',True,0,100,'percent'],
            91: ['OIL_TEMP',True,-10,200,'Celcius'], 92: ['FUEL_INJECT_TIMING',True,-5,25,'degrees'], 93:['FUEL_RATE',True,0,10,'L/hr'], 94: ['unsupported',False] }
#starting the application

def send_PIDs(monitors): #send PIDs to client side
    conn.sendall(b'set_mon')
    conn.recv(BUFFER_SIZE)
    monitors = np.int32(np.array(monitors))#convert to 32 bit
    #conn.sendall(np.int64(np.size(monitors)).tobytes())
    conn.sendall(np.int32(np.size(monitors)).tobytes())
    conn.recv(BUFFER_SIZE)
    conn.sendall(monitors.tobytes())#sending an int array of unknown length
    print(conn.recv(BUFFER_SIZE))

def setup_connection(TCP_IP,TCP_PORT,buffer):
    global conn, s,BUFFER_SIZE
    #TCP_IP = '25.17.212.221'
    BUFFER_SIZE = buffer
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((str(TCP_IP),TCP_PORT))
    s.listen()
    conn, addr = s.accept()
    print('Connection Address: ', addr)
    #conn.sendall(b'start')#tell other side to start

    return 'success'#return status

def main(max_vert,max_hor):
    if np.size(monitors) > max_vert*max_hor: # to  many variables to track
        print('error, too many parameters being tracked')
        conn.close() #end program

    n = int(np.ceil(np.size(monitors)/max_vert)) #this sort of works, should kind of work now...
    m = min(max_vert,np.size(monitors)) # will overshoot the number of subplots needed which is fine, just won't update all of them
    view_window = 10 #this is fairly resonable for now
    #need to make y-limit box based off varying different ranges which will be acceptalbe, I'll keep this for now
    #RPM is easiest to test, so make sure that bounds are big


    fig,axs = plt.subplots(m,n)
    lines = []
    for i in range(n):
        for j  in range(m):
            # we should be good if we can make it here
            if (j+i*m < np.size(monitors)):
                print(j+i*m)
                print(command_table[monitors[j+i*m]])
                ymin = command_table[monitors[j+i*m ]][2]
                ymax = command_table[monitors[j+i*m]][3]
                if n > 1:
                    plot, = axs[j,i].plot([], lw = 1)
                    lines.append(plot)
                    axs[j,i].set_xlim(-1*view_window,0)
                    axs[j,i].set_ylim(ymin,ymax)
                    axs[j,i].set_title(command_table[monitors[j+i*m]][0])
                    axs[j,i].set_xlabel('time')
                    axs[j,i].set_ylabel(command_table[monitors[j+i*m]][4])
                else:#1-d axis plots
                    plot, = axs[j].plot([], lw = 1)
                    lines.append(plot)
                    axs[j].set_xlim(-1*view_window,0)
                    axs[j].set_ylim(ymin,ymax)
                    axs[j].set_title(command_table[monitors[j+i*m]][0])
                    axs[j].set_xlabel('time')
                    axs[j].set_ylabel(command_table[monitors[j+i*m]][4])
    #plt.get_current_fig_manager().window.state('zoomed')
    conn.sendall(b'query')#tell it to start querying
    fig.canvas.draw()

    backgrounds = []
    for i in range(n):
        for j in range(m):
            if (j+i*m < np.size(monitors)):
                if n >1:
                    backgrounds.append(fig.canvas.copy_from_bbox(axs[j,i].bbox))#cache
                else:
                    backgrounds.append(fig.canvas.copy_from_bbox(axs[j].bbox))#cache


    plt.show(block = False) #don't block

    print('starting plots')
    start= 0.
    vals = np.array([])
    times = np.array([])
    count = 0
    last_time = 0
    num_vars = np.size(monitors)
    while 1:
        if len(glob.glob('stop.txt'))>0:
            #tells us to stop
            data = conn.recv(BUFFER_SIZE)
            print('ending')
            conn.sendall(b'end')
            break
        data = conn.recv(BUFFER_SIZE)
        conn.sendall(data)#send back to confirm v
        #t = time.time(), wantto get this from the client side,we don't care about local time
        local_t = time.time() #maybe care about local time
        #update values
        count += 1
        #now want to make sure that all all the values are
        temp = np.frombuffer(data,dtype=np.single)#get all the values
        print(temp)
        vals = np.append(vals,temp[0:num_vars]).reshape(count,num_vars) #this is an expensive thing to keep on having to do
        if count == 1:
            start = temp[num_vars] #still might be weird but less weird

        t = temp[num_vars]#last value should be time
        if len(times) != 0:
            times = times - (t-start -last_time) #shift all values
        last_time = t-start #new adjusted time
        times = np.append(times, [0.])
        for i in range(n):
            for j in range(m):
                if(j+i*m  < num_vars+1):
                    lines[j+i*m].set_data(times,vals[:,j+i*m])
                    if max(times) > view_window:
                        if n > 1:
                            axs[j,i].set_xlim(max(times)-10,max(times))
                        else:
                            axs[j].set_xlim(max(times)-10,max(times))
                    else:
                        if n > 1:
                            axs[j,i].set_xlim(-1*view_window,0)
                        else:
                            axs[j].set_xlim(-1*view_window,0)
                    #axs[i,j].set_ylim(min(vals[:,i+j*m]),max(vals[:,i+j*m]))
                    fig.canvas.restore_region(backgrounds[j+i*m])
                    if n > 1:
                        axs[j,i].draw_artist(lines[j+i*m])
                        fig.canvas.blit(axs[j,i].bbox)
                    else:
                        axs[j].draw_artist(lines[j+i*m])
                        fig.canvas.blit(axs[j].bbox)

        fig.canvas.flush_events()

        if not data: break
        #print("recieved data: :", np.frombuffer(data,dtype=np.int64))
        #print(t-start)
#need to load in the appropriate data
file = open("data_to_send.csv",'rb')
data = pickle.load(file)
max_cols = data[0]
max_rows = data[1]
TCP_IP = data[2]
TCP_PORT = data[3]
BUFFER_SIZE = data[4]
monitors = data[5]
print(data)
setup_connection(TCP_IP,TCP_PORT,BUFFER_SIZE)

print('setting monitors')
send_PIDs(monitors)
print('running main')
main(max_rows,max_cols)
