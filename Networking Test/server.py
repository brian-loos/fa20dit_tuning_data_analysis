import socket
import numpy as np
TCP_IP = '25.17.217.184'
TCP_PORT = 5005
BUFFER_SIZE = 1024
s= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP,TCP_PORT))
s.listen()

conn, addr = s.accept()
print ('Connection Address:', addr)
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    print ("recieved data: ", np.frombuffer(data,dtype=np.int64) )
    #conn.sendall(data)
    #print(type(np.frombuffer(data,dtype=np.int64)))

conn.close()
